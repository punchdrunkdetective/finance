﻿using System;

namespace Finance
{
    /// <summary>
    /// Represents a generic money transaction. Transaction is immuteable to client code but may be manipulated by the client through the use of other 
    /// Finance objects. Transaction was designed to be managed and used by those objects. If this is too constraining, it is recommended to subclass 
    /// Transaction. Subclassing Transaction will expose the state that is protected by the framework, though it is not garranteed that the derived Transaction
    /// will still work with any of its managers defined within the framework. Be sure to check the manager's documentation before using it with a derived class.
    /// Generally, if you plan on using a derived Transaction with a framework manager, you do not want to derive it in such a manner that would allow the client
    /// to break any constraints that the framework manager is responsible to maintianing. 
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Constructs a Transaction with a date, description, and money amount.
        /// </summary>
        /// <param name="date">  The date in which the Transaction was made.             </param>
        /// <param name="descr"> A brief description as to why the Transaction was made. </param>
        /// <param name="amt">   The amount of money associated with the Transaction.    </param>
        public Transaction( DateTime date, String descr, decimal amt )
        {
            this.date  = date;
            this.descr = descr;
            this.amt   = amt;
        }

        /// <summary>
        /// Makes a deep copy of a Transaction.
        /// </summary>
        /// <param name="other"> The other transaction you wish to copy from. </param>
        public Transaction( Transaction other )
        {
            this.date  = other.date;
            this.descr = other.descr;
            this.amt   = other.amt;
        }

        /// <summary>
        /// Represents the date the Transaction was made.
        /// </summary>
        public DateTime date 
        { 
            get; 
            protected internal set; 
        }

        /// <summary>
        /// Represents a breif description of the Transaction.
        /// </summary>
        public String descr 
        { 
            get;
            protected internal set;
        }

        /// <summary>
        /// Represents the amount of money associated with the Transaction.
        /// </summary>
        public decimal amt 
        { 
            get;
            protected internal set;
        }
    }
}

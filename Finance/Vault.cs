﻿using System;
using System.Collections.Generic;

namespace Finance
{
    /// <summary>
    /// Vault is a Savings manager that maintians a collection of Savings and identifies those Savings by name. The only constraint placed upon that collection 
    /// is that no two Savings will share the same name. 
    /// </summary>
    public class Vault
    {
        /// <summary>
        /// Creates an empty Vault.
        /// </summary>
        public Vault()
        {
            _savings = new Dictionary<string, Savings>();
        }

        #region Properties
        /// <summary>
        /// Provides readonly access to the Vault's internal collection of Savings.
        /// </summary>
        protected Dictionary<String, Savings> savings
        {
            get
            {
                return _savings;
            }
        }
        #endregion

        #region Interface
        /// <summary>
        /// Opens a preexisting Savings that is closed in the vault.
        /// </summary>
        /// <param name="name"> A unique identifier specifing the Savings to open. </param>
        /// <returns>
        /// Returns true if a Savings was reopened.
        /// Return false if the Savings was already open or if the Savings doesn't exist.
        /// </returns>
        virtual public bool OpenSavings( String name )
        {
            if (!this.savings.ContainsKey( name ) ||
                this.savings[name].open)
            {
                return false;
            }

            this.savings[name].open = true;
            return true;
        }

        /// <summary>
        /// Opens a new Savings in the Vault. 
        /// </summary>
        /// <param name="name">     A unique identifier specifing the Savings to create. </param>
        /// <param name="openDate"> The open date to iniailize the new Savings with.     </param>
        /// <returns>
        /// Returns true if a Savings was created.
        /// Return false if the Savings already exists.
        /// </returns>
        virtual public bool OpenSavings( String name, DateTime openDate )
        {
            if (this.savings.ContainsKey( name ))
            {
                return false;
            }

            this.savings.Add( name, new Savings( name, openDate) );
            return true;
        }

        /// <summary>
        /// Closes an existing Savings in the Vault. It is important to remember that the Savings will not be removed from the Vault's internal collection. The
        /// Savings open flag is simply unset. 
        /// </summary>
        /// <param name="name"> A unique identifier specifing the Savings to open. </param>
        /// <returns>
        /// Returns true if a Savings was closed.
        /// Returns false if the Savings specified does not exist or was already closed.
        /// </returns>
        virtual public bool CloseSavings( String name )
        {
            if (!this.savings.ContainsKey( name ) ||
                !this.savings[name].open)
            {
                return false;
            }

            this.savings[name].open = false;
            return true;
        }

        /// <summary>
        /// Permantly removes a Savings from the Vault. This method does not close the Savings. It removes all references to the Savings that the Vault may 
        /// have. 
        /// </summary>
        /// <param name="name"> A unique identifier specifing the Savings to open. </param>
        /// <returns>
        /// Returns true if the Savings was removed from the Vault.
        /// Returns false if the Savings specified does not exist.
        /// </returns>
        virtual public bool RemoveSavings( String name )
        {
            if (!this.savings.ContainsKey( name ))
            {
                return false;
            }

            this.savings.Remove( name );
            return true;
        }
        
        /// <summary>
        /// Updates a Savings with a new name. This is regardless to weather the Savings is open or closed.
        /// </summary>
        /// <param name="name">    A unique identifier specifing the Savings to update. </param>
        /// <param name="newName"> A new unique identifier to update the Savings with.  </param>
        /// <returns>
        /// Returns true if the Savings was updated.
        /// Returns false if the Savings specified does not exist or if a Savings with the new name given already exists.
        /// </returns>
        virtual public bool UpdateSavings( String name, String newName )
        {
            if (!this.savings.ContainsKey( name ) ||
                this.savings.ContainsKey( newName ))
            {
                return false;
            }

            Savings selectedSavings = this.savings[name];
            selectedSavings.name = newName;

            this.savings.Remove( name );
            this.savings.Add( newName, selectedSavings );

            return true;
        }

        /// <summary>
        /// Updates a Savings with a new opend date. This is regardless to weather the Savings is open or closed.
        /// </summary>
        /// <param name="name">        A unique identifier specifing the Savings to update.                           </param>
        /// <param name="newOpenDate"> A new open date to update the Savings with. (Only DateTime.Date will be used.) </param>
        /// <returns>
        /// Returns true if the Savings was updated.
        /// Returns false if the Savings specified does not exist or if the new open date is not actually new.
        /// </returns>
        virtual public bool UpdateSavings( String name, DateTime newOpenDate )
        {
            if (!this.savings.ContainsKey( name ) ||
                this.savings[name].openDate == newOpenDate.Date)
            {
                return false;
            }

            this.savings[name].openDate = newOpenDate.Date;
            return true;
        }

        /// <summary>
        /// Provides a shallow copy of the Vault's internal collection of Savings in a Savings Dictionary. 
        /// </summary>
        public Dictionary<String, Savings> GetSavings()
        {
            return new Dictionary<String, Savings>( this.savings );
        }
        #endregion

        #region Backing Store
        private Dictionary<String, Savings> _savings;
        #endregion
    }
}

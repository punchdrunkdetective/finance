﻿using System;
using System.Collections.Generic;

namespace Finance
{
    /// <summary>
    /// Represents an allocation of money, essentially a budget and all expenses made against that budget. For example, Allocation "Food" represents 
    /// a budget of 10 dallors to be used between Tuesday and Saturday. An expense of 15 dallors is made for a burger, well worth the dept, on Wednsday, 
    /// decreasing 10 dallors by 15. 
    /// 
    /// It is important to note that this class is designed to be managed by some Allocation manager defined within the finance framework. Some properties of 
    /// Allocation provide limited access to the client, and manipulation of those properties are delegated to an Alloction manager. This is to help managers 
    /// guarantee certain arbitrary constraints on a collection of Allocations. Examples of such constraints would be:
    ///     1) No two Allocations within a collection should have the same name. 
    ///     2) All Allocations within the collection should have the same date range.
    /// Of coasre, constraints can be added or removed to Allocations by the manager depending on the managers needs and purpose. 
    /// 
    /// If Allocation is too constrained for the client's needs, it is recommended to subclass Allocation. Subclassing Allocation will give access to 
    /// the needed properties and methods that are protected by the framework, though the derived class is not garranteed to work with every manager within
    /// the framework. Be sure to check the manager's documentation before using it with a derived Allocation. A new manager may need to be created by the 
    /// client, if a manager is needed at all. 
    /// </summary>
    public class Allocation
    {
        /// <summary>
        /// Constructs an Allocation with default values. 
        /// </summary>
        /// <param name="name">     A name for the Allocation.               </param>
        /// <param name="initAmt">  The initial amount of the Allocation.           </param>
        /// <param name="fromDate"> The start date for which the Allocation is for. </param>
        /// <param name="toDate">   The end date for which the Allocation is for.   </param>
        /// <exception cref="ArgumentException">
        /// Thrown for one of the following reasons: 
        ///     1) Making an Allocation of negative value. 
        ///     2) Date range given is not valid.
        /// </exception>
        /// <remarks>
        /// Note, the name will only be unique with respect to what manages the Allocation.
        /// The Allocation itself does not garantee the uniqueness of the name.
        /// </remarks>
        public Allocation( String name, decimal initAmt, DateTime fromDate, DateTime toDate )
        {
            if (!(fromDate <= toDate))
            {
                throw new ArgumentException( "Date range given is not valid." );
            }

            this.name    = name;
            this.initAmt = initAmt;
            this.currAmt = initAmt;

            _fromDate = fromDate;
            _toDate   = toDate;
            _expenses = new List<Transaction>();
        }

        #region Properties
        /// <summary>
        /// Allows access to an identifier for the Allocation. 
        /// </summary>
        public String name
        {
            get;
            protected internal set;
        }

        /// <summary>
        /// Represents the amount of money that is initially allocated to represetn an existing budget. This value will never be negative.
        /// By changing the initial amount, the current amount will be adjusted accordingly.
        /// </summary>
        /// <exception cref="ArgumentException"> Setting Allocation to a negative value. </exception>
        public decimal initAmt
        {
            get
            {
                return _initAmt;
            }
            protected internal set
            {
                if (value < 0)
                {
                    throw new ArgumentException( "setting init amount of allocation to negative value", "value" );
                }

                if (value > this.initAmt)
                {
                    decimal diff = value - _initAmt;
                    _initAmt += diff;
                    this.currAmt += diff; // Adjust the current amount to the new init amount. 
                }

                // NOTE: You should not have to worry about the case of when the given value is negative since it is validated above not 
                //       to be negative.
                if (value < _initAmt)
                {
                    decimal diff = _initAmt - value;
                    _initAmt -= diff;
                    this.currAmt -= diff; // Adjust the current amount to the new init amount. 
                }
            }
        }

        /// <summary>
        /// Represents the amount of money left in the allocation available for making expenses. This value will be the value of the initial amount minus 
        /// the sum total of all expenses made against the Allocation. Note that this value can be negative to represent that an Allocation has 
        /// went "over budget".
        /// </summary>
        public decimal currAmt
        {
            get;
            protected set;
        }

        /// <summary>
        /// Represents the beginning of a date range for all valid expenses. The fromDate should always be less than or equal to
        /// the toDate. Whenever fromDate is adjusted to a more recent date, any expense belonging to the Allocation who's date is prior to 
        /// the new fromDate will automatically be removed. 
        /// </summary>
        /// <exception cref="ArgumentException"> Thrown if setting the fromDate would result in fromDate being greater than toDate. </exception>
        public DateTime fromDate
        {
            get
            {
                return _fromDate;
            }
            protected internal set
            {
                if (value.Date > this.toDate)
                {
                    throw new ArgumentException( "Illegal argument for fromDate property." );
                }

                _fromDate = value;

                // Do a inplace removal of expenses.
                for (int i = 0; i < this.expenses.Count; i++)
                {
                    if (this.expenses[i].date < value)
                    {
                        this.currAmt += this.expenses[i].amt;
                        this.expenses.RemoveAt( i );
                        i--; // We need to reprocess this position because remove has renumbered all the elements. 
                    }
                } 
            }
        }

        /// <summary>
        /// Represents the end of a date range for all valid expense. The toDate should always be greater than or equal to
        /// the fromDate. Whenever toDate is adjusted to a prior date, any expense belonging to the Allocation who's date is after 
        /// the new toDate will automatically be removed. 
        /// </summary>
        /// <exception cref="ArgumentException"> Thrown if setting the toDate would result in toDate being less than fromDate. </exception>
        public DateTime toDate
        {
            get
            {
                return _toDate;
            }
            protected internal set
            {
                if (value.Date < this.fromDate)
                {
                    throw new ArgumentException( "Illegal argument for toDate property." );
                }

                _toDate = value;

                // Do a inplace removal of expenses.
                for (int i = 0; i < this.expenses.Count; i++)
                {
                    if (this.expenses[i].date > value)
                    {
                        this.currAmt += this.expenses[i].amt;
                        this.expenses.RemoveAt( i );
                        i--; // We need to reprocess this position because remove has renumbered all the elements. 
                    }
                } 
            }
        }

        /// <summary>
        /// Provides readonly access to the internal list of expenses. The list is returned by reference.
        /// </summary>
        protected List<Transaction> expenses
        {
            get
            {
                return _expenses;
            }
        }
        #endregion

        #region Interface
        /// <summary>
        /// Returns a list of Transactions by shallow copy that represent all expenses made against the Allocation. If you wish to 
        /// change this list, you must do so through the appropriate methods. This property is meant to be just a "reporting" utility
        /// to query the Allocation of what expenses exist in it's collection. This list does not constitute a Transaction history. There is no 
        /// order garranteed in the list returned. What is returned is simply an unordered collection of Transactions.
        /// </summary>
        public List<Transaction> GetExpenses()
        {
            return new List<Transaction>( _expenses );
        }

        /// <summary>
        /// Makes an expense against the Allocation if the expense's date falls within the allocations date
        /// range and the amount given is positive. If sucessfull, this will decrease the current amount of the Allocation.
        /// </summary>
        /// <param name="date">  Represents the date in which the expense was made. (Only the DateTime.Date wil be used.) </param>
        /// <param name="descr"> Describes the reason the expense was made.                                               </param>
        /// <param name="amt">   A positive value that represents how much money the expense was for.                     </param>
        /// <returns> 
        /// Returns true if the expense was sucessfully made.
        /// 
        /// Returns false for one of the following reasons: 
        ///     1) if the date given is not within the date range implied by fromDate and toDate. 
        ///     2) if the amount given was a negative value.
        /// </returns>
        virtual public bool MakeExpense( DateTime when, String desc, decimal amt )
        {
            if (amt < 0)
            {
                return false;
            }

            DateTime date = when.Date;
            if (this.fromDate <= date && date <= this.toDate)
            {
                this.expenses.Add( new Transaction( date, desc, amt ) );
                this.currAmt -= amt;                                 
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method provides the ability to change the date of a specified expense Transaction that belongs to the Allocation's
        /// list of expenses.
        /// </summary>
        /// <param name="index"> An index marking the location of the expense within the Allocation's expense list. </param>
        /// <param name="when">  A date to update the expense with.                                                 </param>
        /// <returns>
        /// Returns true if the expense was updated sucessfully. 
        /// 
        /// Returns false for one of the following reasons:
        ///     1) index is outside the bounds of expense list.
        ///     2) date given is outside the date range specified by fromDate and toDate.
        /// </returns>
        virtual public bool UpdateExpense( int index, DateTime newDate )
        {
            if ((index < 0 || index >= this.expenses.Count) ||
                (newDate < this.fromDate || newDate > this.toDate))
            {
                return false;
            }

            this.expenses[index].date = newDate;
            return true;
        }

        /// <summary>
        /// This method provides the ability to change the description of a specified expense Transaction that belongs to the Allocation's
        /// list of expenses. 
        /// </summary>
        /// <param name="index"> An index marking the location of the expense within the Allocation's expense list. </param>
        /// <param name="desc">  A description to update the expense with.                                          </param>
        /// <returns>
        /// Returns true if the expense was updated sucessfully. 
        /// 
        /// Returns false for one of the following reasons:
        ///     1) index is outside the bounds of expense list.
        /// </returns>
        virtual public bool UpdateExpense( int index, String newDesrc )
        {
            if (index < 0 || index >= this.expenses.Count)
            {
                return false;
            }

            this.expenses[index].descr = newDesrc;
            return true;
        }

        /// <summary>
        /// This method provides the ability to change the amount of a specified expense Transaction that belongs to the Allocation's
        /// list of expenses.
        /// </summary>
        /// <param name="index"> An index marking the location of the expense within the Allocation's expense list. </param>
        /// <param name="amt">   A amount to update the expense with.                                               </param>
        /// <returns>
        /// Returns true if the expense was updated sucessfully. 
        /// 
        /// Returns false for one of the following reasons:
        ///     1) index is outside the bounds of expense list.
        ///     2) amt given is negative.
        /// </returns>
        virtual public bool UpdateExpense( int index, decimal newAmt )
        {
            if ((index < 0 || index >= this.expenses.Count) ||
                newAmt < 0)
            {
                return false;
            }

            decimal amtDff = Math.Abs( this.expenses[index].amt - newAmt );
            if (this.expenses[index].amt < newAmt)
            {
                this.currAmt -= amtDff;
            }
            else if (this.expenses[index].amt > newAmt)
            {
                this.currAmt += amtDff;
            }

            this.expenses[index].amt = newAmt;
            return true;
        }

        /// <summary>
        /// Removes an expense from the Allocation. If sucessfull, this will increase the current amount of the Allocation by the amount of 
        /// the removed expense. 
        /// </summary>
        /// <param name="index"> An index in the Allocation's list of expenses, indicating the expense to remove. </param>
        /// <returns> 
        /// Returns true if the expense was sucessfully removed.
        /// Returns false if the index given is out of the Allocation's expense list bounds. 
        /// </returns>
        virtual public bool RemoveExpense( int index )
        {
            if (index < 0 || index >= this.expenses.Count)
            {
                return false;
            }

            this.currAmt += this.expenses[index].amt;
            this.expenses.RemoveAt( index );
            return true;
        }
        #endregion

        #region Backing Store
        private decimal  _initAmt;
        private DateTime _fromDate;
        private DateTime _toDate;
        private List<Transaction> _expenses;
        #endregion
    }
}

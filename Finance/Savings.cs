﻿using System;
using System.Collections.Generic;

namespace Finance
{
    /// <summary>
    /// Represents an acumulation of money over time. Savings was designed to represent a real world savings account that you may have with a bank. 
    /// Money can be acumulated through making deposits, as well as taken away from through making withdrawals as you would with any normal bank account. 
    /// 
    /// The Savings will maintain two lists of Transactions, each of which represent all withdrawals and all deposites made respectively.
    /// These Transactions can only be made if the Savings' open flag is set. A running total of the Savings' acumulation will be maintained, which
    /// will never hit below zero. You cannot overdraw from a Savings. The current amount of the Savings will always be the total of deposits minus
    /// the total of withdrawals. 
    /// 
    /// It is important to note that even though Transactions are maintianed in lists, no Transaction history can be generated through these lists.
    /// The Transaction lists do not garantee an order in which Transactions were made. They are simply a collection of Transactions, who's only order
    /// implied is by their index in their respective list. This has a couple of interesting consequences that should probably be addressed in subsequent
    /// releases. Since there is no Transaction history to validate against, the only thing about a Transaction that can be validated is it's amount so
    /// that the constraint, that Savings current amount is never below zero, is maintained. The Transaction's date is never questioned, allowing 
    /// you to do things like make a deposit of 20 dallors on the 2nd and then later make a withdrawal for 10 dallors on the 1st. 
    /// 
    /// It is important to note that this class is designed to be managed by some Savings manager defined within the finance framework. Some properties of 
    /// Savings provide limited access to the client, and manipulation of those properties are delegated to a Savings manager. This is to help managers 
    /// guarantee certain arbitrary constraints on a collection of Savings. If Savings is too constrained for the client's needs, it is recommended to 
    /// subclass Savings. Subclassing Savings will give access to the needed properties and methods that are protected by the framework, though the derived 
    /// class is not garranteed to work with every manager within the framework. Be sure to check the manager's documentation before using it with a derived
    /// Savings. A new manager may need to be created by the  client, if a manager is needed at all. 
    /// </summary>
    public class Savings
    {
        // TODO: Make a Transaction history of sorts to validate Transactions against. (big problem! low priority...)

        // TODO: Write a Savings manager that would maintian a collection of Savings and enforce the constraint that all Savings in that collection
        //       will be unique. You may want to even make it a singleton to make sure there is only one such manager, only one collection of Savings.
        //       Though this may be better left off for the client to decide. (top priority)

        /// <summary>
        /// Constructs a Savings with a unique identifier. The initial amount of the Savings will be set to 0.00 dallors with no 
        /// transactions. By defualt, the Savings will be open. No Transaction can be made with the Savings befor the open date given. 
        /// </summary>
        /// <param name="name">     A unique name for the Savings.                                                                     </param>
        /// <param name="openDate"> The date in which the Savings was first opened. All Transaction must happen on or after this date. </param>
        /// <remarks>
        /// Note, that the uniqueness of the identifier is not garanteed by the class itself. It is the responsibiality of whatever is managing 
        /// the Savings to make the identifier unique. 
        /// </remarks>
        public Savings( String name, DateTime openDate )
        {
            this.name = name;
            this.open = true;

            _currAmt     = 0.00m;
            _openDate    = openDate;
            _withdrawals = new List<Transaction>();
            _deposits    = new List<Transaction>();
        }

        /// <summary>
        /// Constructs a Savings with the given name, open date, and intial amount. A deposit will be automatically added to represent the
        /// initial amount given. The deposit will have the same date as the open date and a description of "Initial deposit." The withdrawals
        /// list will remain empty. 
        /// </summary>
        /// <param name="name">     A unique name for the Savings.                                                                     </param>
        /// <param name="openDate"> The date in which the Savings was first opened. All Transaction must happen on or after this date. </param>
        /// <param name="initAmt">  An amount of money that the Savings is open with.                                                  </param>
        /// <remarks>
        /// Note, that the uniqueness of the identifier is not garanteed by the class itself. It is the responsibiality of whatever is managing 
        /// the Savings to make the identifier unique. 
        /// </remarks>
        public Savings( String name, DateTime openDate, decimal initAmt )
        {
            this.name = name;
            this.open = true;

            _currAmt     = 0.00m;
            _openDate    = openDate;
            _withdrawals = new List<Transaction>();
            _deposits    = new List<Transaction>();

            this.MakeDeposit( _openDate, "Initial deposit.", initAmt );
        }

        #region Properties
        /// <summary>
        /// Allows access to the unique identifier for a Savings. The uniqueness of the name should be maintained by the finance framework. Specifically, any 
        /// Savings collection of the framework that manages the Allocation should garantee that the name of the Allocation will be unique within that collection. 
        /// </summary>
        public string name
        {
            get;
            protected internal set;
        }

        /// <summary>
        /// Gets or sets the Savings' open flag. Withdrawals or deposits can only be made if the Savings
        /// is open. Note though, that withdrawals and deposits can be updated despite whatever value the open
        /// flag is. 
        /// </summary>
        public bool open
        {
            get;
            protected internal set;
        }

        /// <summary>
        /// Represents the current amount of money in the Savings. This value will never be below zero and should always equal the sum total of 
        /// deposits minus the sum total of withdrawals. 
        /// </summary>
        /// <exception cref="ArgumentException"> Thrown by the setter, if the value given is negative. </exception>
        public decimal currAmt 
        {
            get
            {
                return _currAmt;
            }
            protected set
            {
                if (value < 0)
                {
                    throw new ArgumentException( "Current amount of a Savings is set below zero." );
                }

                _currAmt = value;
            }
        }

        /// <summary>
        /// Represents the date in which the Savings was first opened. All Transactions of the Savings will be on or after this date. Changing this value to
        /// a more recent date may invalidate withdrawals or deposits that occured before the new date. In that case, those withdrawals and deposits will be
        /// automatically removed from their respective lists. 
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Thrown if the new value of the open date invalidates Transactions and the removal of those Transactions would put the Savings in a negative
        /// balance. It is recommended that Transactions be updated with new valid dates before setting the open date to a more recent date.
        /// </exception>
        public DateTime openDate
        {
            get
            {
                return _openDate;
            }
            protected internal set
            {
                // Calculate what the current amount will be before acutally setting the new current amount.
                decimal newCurrAmt = this.currAmt;

                // Do a inplace removal of deposits.
                decimal totalDepositRemoved = 0.00m;
                for (int i = 0; i < this.deposits.Count; i++)
                {
                    if (this.deposits[i].date < value)
                    {
                        totalDepositRemoved += this.deposits[i].amt;
                    }
                }
                newCurrAmt -= totalDepositRemoved; // Removal of deposits implies an decrease of money.

                // Do a inplace removal of withdrawals.
                decimal totalWithdrawalRemoved = 0.00m;
                for (int i = 0; i < this.withdrawals.Count; i++)
                {
                    if (this.withdrawals[i].date < value)
                    {
                        totalWithdrawalRemoved += this.withdrawals[i].amt;
                    }
                }
                newCurrAmt += totalWithdrawalRemoved; // Removal of withdrawals implies an increase of money.

                if (newCurrAmt >= 0)
                {
                    // Actually remove the deposits.
                    for (int i = 0; i < this.deposits.Count; i++)
                    {
                        if (this.deposits[i].date < value)
                        {
                            this.deposits.RemoveAt( i );
                            i--; // We need to reprocess this position because remove has renumbered all the elements. 
                        }
                    }

                    // Actually remove the withdrawals.
                    for (int i = 0; i < this.withdrawals.Count; i++)
                    {
                        if (this.withdrawals[i].date < value)
                        {
                            this.withdrawals.RemoveAt( i );
                            i--; // We need to reprocess this position because remove has renumbered all the elements. 
                        }
                    }

                    this.currAmt = newCurrAmt;
                    _openDate = value;
                }
                else
                {
                    throw new ArgumentException( "New openDate value invalidates Transactions in such a way to put the Savings in a negative balance." );
                }
            }
        }

        /// <summary>
        /// Provides readonly access to the Savings internal list of withdrawal Transactions.
        /// </summary>
        protected List<Transaction> withdrawals
        {
            get
            {
                return _withdrawals; 
            }
        }

        /// <summary>
        /// Provides readonly access to the Savings internal list of deposit Transactions. 
        /// </summary>
        protected List<Transaction> deposits
        {
            get
            {
                return _deposits;
            }
        }
        #endregion

        #region Interface
        /// <summary>
        /// Utility that indicates if the given index is within the range of the withdrawals list. 
        /// </summary>
        public bool IsValidWithdrawalIndex( int index )
        {
            return 0 <= index && index < this.withdrawals.Count;
        }

        /// <summary>
        /// Utility that indicates if the given index is within the range of the deposits list. 
        /// </summary>
        public bool IsValidDepositIndex( int index )
        {
            return 0 <= index && index < this.deposits.Count;
        }

        /// <summary>
        /// Returns a shallow copy of a Transactions list that represents all deposits made to the Savings. If you wish to 
        /// modify the actual list, you must do so through the appropriate methods. This property is provided mearly as a "reporting"
        /// utility, for querying what deposits currently exist. This list does not constitute a Transaction history. There is no 
        /// order garranteed in the list returned. What is returned is simply an unordered collection of Transactions. 
        /// </summary>
        public List<Transaction> GetDeposits()
        {
            return new List<Transaction>( _deposits ); 
        }

        /// <summary>
        /// Returns a shallow copy of a Transactions list that represents all withdrawals taken from the Savings. If you wish to 
        /// modify the actual list, you must do so through the appropriate methods. This property is provided mearly as a "reporting"
        /// utility, for querying what withdrawals currently exist. This list does not constitute a Transaction history. There is no 
        /// order garranteed in the list returned. What is returned is simply an unordered collection of Transactions. 
        /// </summary>
        public List<Transaction> GetWithdrawals()
        {
            return new List<Transaction>( _withdrawals ); 
        }

        /// <summary>
        /// Makes a withdrawal from the Savings if the Savings is open, the amount given is positive, and the transaction would not put the Savings in
        /// a negative balance. If sucessfull, this will reduce the current amount of the Savings by the amount given and return true. 
        /// </summary>
        /// <param name="when">  Represents the date in which the withdrawal was made. (Only the DateTime.Date wil be used). </param>
        /// <param name="descr"> Describes the reason the withdrawal was made.                                               </param>
        /// <param name="amt">   A positive value that represents how much money was withdrawn.                              </param>
        /// <returns> 
        /// Returns true if the withdrawal sucessfully took place. 
        /// 
        /// Returns false for one of the following reasons: 
        ///     1) if the Savings is not open
        ///     2) if the amount given is negative
        ///     3) if the withdrawal would put Savings in a negative balance. 
        ///     4) if the date given is before the open date of the Savings.
        /// </returns>
        /// <remarks>
        /// Note that the date and amount ares not validated against a Transaction history. If a Transaction history is generated, the new ordering of withdrawals
        /// in that history could show a Savings going into an inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool MakeWithdrawal( DateTime when, string descr, decimal amt )
        {
            if (amt < 0.00m                ||
                this.open == false         ||
                this.currAmt - amt < 0.00m ||
                this.openDate > when)
            {
                return false;
            }

            DateTime date = when.Date;
            this.withdrawals.Add( new Transaction( date, descr, amt ) ); 
            this.currAmt -= amt;                                     
            return true;                                             
        }

        /// <summary>
        /// Makes a deposit to the Savings if the Savings is open  and the amount given is positive. If sucessfull, this will increase the 
        /// current amount of the Savings by the amount given and return true. 
        /// </summary>
        /// <param name="date">  Represents the date in which the deposit was made. (Only the DateTime.Date wil be used). </param>
        /// <param name="descr"> Describes the reason the deposit was made.                                               </param>
        /// <param name="amt">   A positive value that represents how much money was deposited.                           </param>
        /// <returns> 
        /// Returns true if the deposit sucessfully took place.
        /// 
        /// Return false for one of the following reasons:
        ///     1) if the Savings is not open.
        ///     2) if the amount given is negative. 
        ///     3) if the date given is before the open date of the Savings.
        /// </returns>
        /// <remarks>
        /// Note that the date and amount ares not validated against a Transaction history. If a Transaction history is generated, the new ordering of withdrawals
        /// in that history could show a Savings going into an inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool MakeDeposit( DateTime when, string descr, decimal amt )
        {
            // NOTE: The Transaction history does not matter when making a deposit. As long as the Savings is in a valid state before making  
            //       the new deposit, no matter what the date of that deposit is, the Transaction history will not show invalid Savings state.

            if (amt < 0            ||
                this.open == false ||
                this.openDate > when)
            {
                return false;
            }

            DateTime date = when.Date;
            this.deposits.Add( new Transaction( date, descr, amt ) ); 
            this.currAmt += amt;                                  
            return true;                                           
        }

        /// <summary>
        /// Provides interface to update the date of a withdrawal in the Savings' list. 
        /// </summary>
        /// <param name="index"> Specifies the withdrawal in the Savings list to be updated.                              </param>
        /// <param name="date">  The new date the withdrawal is to be updated with. (Only the DateTime.Date will be used. </param>
        /// <returns>
        /// Return true if the date was sucessfully updated.
        /// 
        /// Return false for one of the following reasons: 
        ///     1) if the index given is outside the bounds of the withdrawal list. 
        ///     2) if the date given is before the Savings open date.
        /// </returns>
        /// <remarks>
        /// Note that the new date is not validated against a Transaction history. If a Transaction history is generated, the new ordering of withdrawals
        /// in that history could show a Savings going into an inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool UpdateWithdrawal( int index, DateTime newDate )
        {
            if (this.IsValidWithdrawalIndex( index ) == false ||
                this.openDate > newDate)
            {
                return false;
            }

            this.withdrawals[index].date = newDate;
            return true;
        }

        /// <summary>
        /// Provides interface to update the description of a withdrawal in the Savings' list. 
        /// </summary>
        /// <param name="index"> Specifies the withdrawal in the Savings list to be updated. </param>
        /// <param name="descr"> The new description the withdrawal is to be updated with.   </param>
        /// <returns>
        /// Return true if the description was sucessfully updated.
        /// Return false if the index given is outside the bounds of the withdrawal list. 
        /// </returns>
        virtual public bool UpdateWithdrawal( int index, String newDescr )
        {
            if (this.IsValidWithdrawalIndex( index ) == false)
            {
                return false;
            }

            this.withdrawals[index].descr = newDescr;
            return true;
        }

        /// <summary>
        /// Provides interface to update the amount of a withdrawal in the Savings list. If sucessfull, 
        /// the current amount of the Savings will be updated accordingly. 
        /// </summary>
        /// <param name="index"> Specifies the withdrawal in the Savings list to be updated. </param>
        /// <param name="amt">   The new amount the withdrawal is to be updated with.        </param>
        /// <returns>
        /// Return true if the amount was sucessfully updated.
        /// 
        /// Return false for one of the following reasons: 
        ///     1) if the index given is outside the bounds of the withdrawal list
        ///     2) if the amount given is negative
        ///     3) if the amount given would put the Savings in a negative balance 
        /// </returns>
        /// <remarks>
        /// Note that the only thing the new amount is validated against is the current amount of the Savings at the time of the update, NOT
        /// the current amount of the Savings at the time when the Transaction was first made. If a Transaction history is generated, the new amount might
        /// show the Savings going into a inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool UpdateWithdrawal( int index, decimal newAmt )
        {
            if (this.IsValidWithdrawalIndex( index ) == false ||
                newAmt < 0)
            {
                return false;
            }

            try
            {
                if (newAmt > this.withdrawals[index].amt)
                {
                    // An increase in withdrawal decreases the saving's total amount.
                    this.currAmt -= newAmt - this.withdrawals[index].amt;
                }
                else if (newAmt < this.withdrawals[index].amt)
                {
                    // A descrease in withdrawal will increase the saving's total amount.
                    this.currAmt += this.withdrawals[index].amt - newAmt;
                }
                this.withdrawals[index].amt = newAmt;
            }
            catch (ArgumentException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Provides interface to update the date of a desposit in the Savings list. 
        /// </summary>
        /// <param name="index">   Specifies the deposit in the Savings list to be updated.                       </param>
        /// <param name="newWhen"> The new date to update the deposit with. (Only the DateTime.Date will be used. </param>
        /// <returns>
        /// Returns true if the update was sucessfull. 
        /// 
        /// Returns false for one of the following reasons: 
        ///     1) if the index is out of the deposit list range. 
        ///     2) if the new date is before the open date of the Savings.
        /// </returns>
        /// <remarks>
        /// Note that the new date is not validated against a Transaction history. If a Transaction history is generated, the new ordering of deposits
        /// in that history could show a Savings going into an inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool UpdateDeposit( int index, DateTime newDate )
        {
            if (this.IsValidDepositIndex( index ) == false || 
                this.openDate > newDate)
            {
                return false;
            }

            this.deposits[index].date = newDate.Date;
            return true;
        }

        /// <summary>
        /// Provides interface to update the description of a desposit in the Savings list. 
        /// </summary>
        /// <param name="index">    Specifies the deposit in the Savings list to be updated. </param>
        /// <param name="newDescr"> The new description to update the deposit with.          </param>
        /// <returns>
        /// Returns true if the update was sucessfull. 
        /// Returns false if the index is out of the deposit list range. 
        /// </returns>
        virtual public bool UpdateDeposit( int index, String newDescr )
        {
            if (this.IsValidDepositIndex( index ) == false)
            {
                return false;
            }

            this.deposits[index].descr = newDescr;
            return true;
        }

        /// <summary>
        /// Provides interface to update the amount of a desposit in the Savings list. 
        /// </summary>
        /// <param name="index">  Specifies the deposit in the Savings list to be updated. </param>
        /// <param name="newAmt"> The new amount to update the deposit with.               </param>
        /// <returns>
        /// Returns true if the update was sucessfull. 
        /// 
        /// Returns false for one of the following reasons:
        ///     1) if the index is out of the deposit list range
        ///     2) the new amount is negative
        /// </returns>
        /// <remarks>
        /// Note that the new amount is not validated against a Transaction history. If a Transaction history is generated, the new ordering of deposits
        /// in that history could show a Savings going into an inconsistent state. This can be missleading and probably should be fixed in subsequent releases. 
        /// </remarks>
        virtual public bool UpdateDeposit( int index, decimal newAmt )
        {
            if (this.IsValidDepositIndex( index ) == false ||
                newAmt < 0 )
            {
                return false;
            }

            this.deposits[index].amt = newAmt;
            return true;
        }
        
        /// <summary>
        /// Removes a withdrawal from the Savings. If sucessfull, the Savings current amount will increase by the amount of the withdrawal removed.
        /// </summary>
        /// <param name="index"> Specifies the withdrawal to remove in the internal list of withdrawals. </param>
        /// <returns>
        /// Returns true if the sucessfull.
        /// Returns false if the index given is out of the withdrawals list bounds. 
        /// </returns>
        virtual public bool RemoveWithdrawal( int index )
        {
            if (this.IsValidWithdrawalIndex( index ) == false)
            {
                return false;
            }

            this.currAmt += this.withdrawals[index].amt;
            this.withdrawals.RemoveAt( index );
            return true;
        }

        /// <summary>
        /// Removes a deposit from the Savings. If sucessfull, the Savings current amount will decrease by the amount of the withdrawal removed.
        /// </summary>
        /// <param name="index"> Specifies the deposit to remove in the internal list of deposits. </param>
        /// <returns>
        /// Returns true if the sucessfull.
        /// 
        /// Returns false for one of the following reasons:
        ///     1) if the index given is out of the deposits list bounds. 
        ///     2) if removing the deposit will but the Savings in a negative balance.
        /// </returns>
        virtual public bool RemoveDeposit( int index )
        {
            if (this.IsValidDepositIndex( index ) == false ||
                this.currAmt - this.deposits[index].amt < 0)
            {
                return false;
            }

            this.currAmt -= this.deposits[index].amt;
            this.deposits.RemoveAt( index );
            return true;
        }
        #endregion

        #region Backing Store
        private decimal           _currAmt;
        private DateTime          _openDate;
        private List<Transaction> _withdrawals;
        private List<Transaction> _deposits;
        #endregion
    }
}

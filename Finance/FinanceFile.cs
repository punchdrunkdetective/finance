﻿using System;
using System.Collections.Generic;

namespace Finance
{
    /// <summary>
    /// FinanceFile is a Allocation manager that manages a dictionary of Allocations and enforces the following constraints:
    ///     1) No two Allocations in it's collection will have the same name.
    ///     2) Every Allocation in it's collection wil have the same date range. 
    ///     3) The sum total of all Allocations initAmt properties will equal a specified amount and will not go over that amount. 
    ///     
    /// FinanceFile can be said to manage an income or money pool for a specific time period, that manages that money by budgeting it out into several
    /// Allocations. A use case may be illuminating. A client has a 1000 dallors to spend for a week. To place limits on his or her spending behavior, the
    /// client would like to budget 100 dallors for eating-out and 200 dallors for groceries out of that 1000 dallors, leaving 800 dallors unallocated to 
    /// be used for anything else during that week that might come. The client would then have three budgets for that week where Transactions 
    /// are recorded for. 
    /// </summary>
    public class FinanceFile
    {
        /// <summary>
        /// Constructs a FinanceFile with default values.
        /// </summary>
        /// <param name="initAmt">  The inittial total amount of money the FinancialFile can allocate and manage.                                          </param>
        /// <param name="fromDate"> The beginning of the transaction range. Should be less than or equal to toWhen. (Only the DateTime.Date will be used.) </param>
        /// <param name="toDate">   The end of the transaction range. Should be greater than or equal to fromWhen. (Only the DateTime.Date will be used.)  </param>
        /// <exception cref="ArgumentException"> 
        /// Thrown for one of the following reasons:
        ///     1) Creating FinanceFile with negative intial amount. 
        ///     2) The date range given is not a valid range.
        /// </exception>
        public FinanceFile(  DateTime fromDate, DateTime toDate, decimal initAmt ) 
        {
            if (fromDate.Date > toDate.Date ||
                initAmt < 0)
            {
                throw new ArgumentException( "Invalid construction parameters." );
            }

            _fromDate = fromDate;
            _toDate   = toDate;

            _moneyPool   = initAmt;
            _allocated   = 0.00m;
            _unallocated = initAmt;

            _allocations = new Dictionary<string, Allocation>();
        }

        #region Properties
        /// <summary>
        /// Represents the beginning of a date range. Every Allocation managed by the FinanceFile will have this date range. Changing this value will
        /// change the fromDate field of every Allocation in FinanceFile.
        /// </summary>
        /// <exception cref="ArgumentException"> Thrown if setting the fromDate would result in fromDate being greater than toDate. </exception>
        /// <remarks>
        /// Only the DateTime.Date is used in the setter. 
        /// </remarks>
        public DateTime fromDate
        {
            get
            {
                return _fromDate;
            }
            set
            {
                if (value.Date > this.toDate)
                {
                    throw new ArgumentException( "Illegal argument for fromDate property." );
                }

                _fromDate = value.Date;
                foreach( KeyValuePair<String, Allocation> allocPair in this.allocations )
                {
                    allocPair.Value.fromDate = value;
                }
            }
        }

        /// <summary>
        /// Represents the end of a date range. Every Allocation managed by the FinanceFile will have this date range. Changing this value will
        /// change the toDate field of every Allocation in FinanceFile. 
        /// </summary>
        /// <exception cref="ArgumentException"> Thrown if setting the toDate would result in toDate being less than fromDate. </exception>
        /// <remarks>
        /// Only the DateTime.Date is used in the setter. 
        /// </remarks>
        public DateTime toDate
        {
            get
            {
                return _toDate;
            }
            set
            {
                if (value.Date < this.fromDate)
                {
                    throw new ArgumentException( "Illegal argument for toDate property." );
                }

                _toDate = value.Date;
                foreach (KeyValuePair<String, Allocation> allocPair in this.allocations)
                {
                    allocPair.Value.toDate = value;
                }
            }
        }

        /// <summary>
        /// Represents the total amount of money managed by the FinanceFile, allocated or not. This value can only be positive. Adjusting this
        /// value will also adjust the unalocated value respectively. Increasing the moneyPool will increase the unallocated value and vise versa. 
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Thrown if setting the moneyPool to a negative value or if setting the value would result in the moneyPool being less than 
        /// the value of allocated. 
        /// </exception>
        public decimal moneyPool
        {
            get
            {
                return _moneyPool;
            }
            set
            {
                if( value < 0 ||
                    value < this.allocated)
                {
                    throw new ArgumentException( "Illegal argument for moneyPool property." ); 
                }

                decimal amtDff = Math.Abs( _moneyPool - value );
                if (_moneyPool > value)
                {
                    _moneyPool       -= amtDff;
                    this.unallocated -= amtDff;
                }
                else if (_moneyPool < value)
                {
                    _moneyPool       += amtDff;
                    this.unallocated += amtDff;
                }
            }
        }

        /// <summary>
        /// Represents the total amount of money that has already been allocated. This value can only be positive and is equivalent to 
        /// moneyPool - unallocated. It can also be viewed as the sum total of all Allocation's initAmt properties. 
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Thrown if setting allocated to a negative value or if setting the value would result in allocated being greater than 
        /// the value of the moneyPool. 
        /// </exception>
        /// <remarks>
        /// No side effects incure when setting this value. It is the responsibility of the class or it's subclasses to cause desired side effects. For example, 
        /// one desired side effect would be that when increasing the allocated value, the unallocated value would decrease. The reason such side effects are not
        /// implement within the property is that an endless cycle of side effect would incure till an exception is thrown. Better to just to just let other
        /// methods handle that. 
        /// </remarks>
        public decimal allocated
        {
            get
            {
                return _allocated;
            }
            protected set
            {
                if (value < 0 ||
                    value > this.moneyPool)
                {
                    throw new ArgumentException( "Illegal argument for allocated property." );
                }

                _allocated = value;
            }
        }

        /// <summary>
        /// Represents the total amount of money that has not been allocated yet. This value can only be positive and is equivalent to 
        /// moneyPool - allocated.
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Thrown if setting unallocated to a negative value or if setting the value would result in unallocated being greater than 
        /// the value of the moneyPool. 
        /// </exception>
        /// <remarks>
        /// No side effects incure when setting this value. It is the responsibility of the class or it's subclasses to cause desired side effects. For example, 
        /// one desired side effect would be that when increasing the allocated value, the unallocated value would decrease. The reason such side effects are not
        /// implement within the property is that an endless cycle of side effect would incure till an exception is thrown. Better to just to just let other
        /// methods handle that. 
        /// </remarks>
        public decimal unallocated
        {
            get
            {
                return _unallocated;
            }
            protected set
            {
                if (value < 0 ||
                    value > this.moneyPool)
                {
                    throw new ArgumentException( "Illegal argument for unallocated property." );
                }

                _unallocated = value;
            }
        }

        /// <summary>
        /// Represents the total amount of money spent across all Allocations contained in the FinanceFile. It will equal the sum total of those Allocation's
        /// currAmt property. This value can only be positive. 
        /// </summary>
        public decimal totalSpent
        {
            get
            {
                decimal totalExpenses = 0.00m;
                foreach (KeyValuePair<String, Allocation> allocTuple in this.allocations)
                {
                    totalExpenses += allocTuple.Value.initAmt - allocTuple.Value.currAmt;
                }

                return totalExpenses;
            }
        }

        /// <summary>
        /// Represents the current balance of the FinanceFile, you much money you have left to spend in general. It is calculated by subtracting totalSpent
        /// from moneyPool. This value can be negative to indicate dept.
        /// </summary>
        public decimal balance
        {
            get
            {
                return this.moneyPool - this.totalSpent;
            }
        }

        /// <summary>
        /// Allows readonly access to the internal dicationary of Allocations by reference. 
        /// </summary>
        protected Dictionary<string, Allocation> allocations
        {
            get
            {
                return _allocations; 
            }
        }
        #endregion

        #region Interface
        /// <summary>
        /// Returns a Dictionary of all Allocations in the FinanceFile by shallow copy where the keys are the Allocation's names. If you wish
        /// to manipulate the actual collection of Allocations you must do so in the approbiate methods. This property is provided mearly as 
        /// a "reporting" utility to query the FinanceFile of what Allocations exist, and to provide a means to interact with those Allocations.
        /// </summary>
        public Dictionary<string, Allocation> GetAllocations()
        {
            return new Dictionary<string, Allocation>( _allocations ); 
        }

        /// <summary>
        /// Allocates a portion of the FinanceFile's budget by making a new Allocation with the name and the amount given. This will only be done
        /// if the amount specified is less than or equal to the unallocated property. If the Allocation can be made, unallocated will decrease in 
        /// value as allocated increases in value. The Allocation will be added to the FinanceFile's internal list of Allocations and have
        /// the same date range as the FinanceFile. 
        /// </summary>
        /// <param name="name"> A name that uniquelly identifies the new allocation within the FinanceFile's internal dictionary. </param>
        /// <param name="amt">  The amount of money the new allocation is for.                                                    </param>
        /// <return> 
        /// Returns true if the Allocation was made.
        /// 
        /// Returns false for one of the following reasons: 
        ///     1) An allocation with the given name already exists in the FinanceFile's dictionary. 
        ///     2) The amount given exceeds the value of unallocated.
        ///     3) The amount given is negative. 
        /// </return>
        virtual public bool MakeAllocation( String name, decimal amt)
        {
            if ((amt  <   0        || amt > this.unallocated) ||
                (this.allocations.ContainsKey( name )))
            {
                return false;
            }

            this.allocations.Add( name, new Allocation( name, amt, this.fromDate, this.toDate ) ); 

            this.unallocated -= amt;
            this.allocated   += amt;
                        
            return true;                                                                       
        }

        /// <summary>
        /// Adjusts the intial amount of an Allocation. The Allocation can only be adjusted if the difference between the new value and old value 
        /// is less than or equal to unallocated. If you are decreasing the Allocation, FinanceFile's allocated amount will increase while unallocated 
        /// decreases. If you are increasing the Allocation, FinanceFile's allocated amount will decrease while unallocated increases. 
        /// </summary>
        /// <param name="name"> The name of the Allocation that you wish to adjust.               </param>
        /// <param name="amt">  The new value of the Allocation's initAmt. This must be positive. </param>
        /// <returns>
        /// Returns true if the Allocation was adjusted sucessfully. 
        /// 
        /// Returns false for one of the following reasons: 
        ///     1) if the amount given is the same as the Allocation's current initial amount.
        ///     2) if the amount given is negative.
        ///     3) if the name given doesn't specify an Allocation in the FinanceFile's dictionary.
        ///     4) if the unallocated amount of the FinanceFile is not great enough to make the specified adjustment.
        /// </returns>
        virtual public bool UpdateAllocation( String name, decimal toAmt )
        {
            if (this.allocations.ContainsKey( name ) == false ||
                toAmt < 0) 
            {
                return false;
            }

            Allocation targetAllocation = this.allocations[name];
            if (targetAllocation.initAmt == toAmt)
            {
                return false;
            }

            decimal adjustAmt = Math.Abs( this.allocations[name].initAmt - toAmt ); // the amount you are adjusting by
            bool    success   = false;
            if ((targetAllocation.initAmt < toAmt) &&
                (adjustAmt <= unallocated))
            {
                this.unallocated -= adjustAmt;
                this.allocated   += adjustAmt;

                targetAllocation.initAmt = toAmt;
                success = true;
            }
            else if (targetAllocation.initAmt > toAmt)
            {
                this.unallocated += adjustAmt;
                this.allocated   -= adjustAmt;

                targetAllocation.initAmt = toAmt;
                success = true;
            }

            return success;
        }

        /// <summary>
        /// Adjusts the name of an Allocation, if the new name doesn't already exist in the FinanceFile's dictionary.
        /// </summary>
        /// <param name="name">    Identifies the Allocation to be changed.  </param>
        /// <param name="newName"> The new name of the specified Allocation. </param>
        /// <returns>
        /// Returns true if the specified Allocation could and was changed to the given name.
        /// 
        /// Returns false if the specified Allocation could NOT be changed to the given name for one of the following reasons:
        ///     1) there is already an Allocation managed by the FinanceFile with the new name.
        ///     2) the specified Allocation already has the same name as the new name. 
        ///     3) the specified Allocaton doesn't exist.
        /// </returns>
        virtual public bool UpdateAllocation( String name, String newName )
        {
            if (this.allocations.ContainsKey( name )    == true &&
                this.allocations.ContainsKey( newName ) == false)
            {
                Allocation a = this.allocations[name];
                this.allocations.Remove( name ); 

                a.name = newName;
                this.allocations.Add( newName, a );

                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes an Allocation specified by name from the FinanceFile's inteneral Dictionary of Allocations. If sucessfull, the FinanceFile's unallocated
        /// amount will increase while the allocated amount decreases. 
        /// </summary>
        /// <param name="name"> Identifies the Allocation to remove. </param>
        /// <returns>
        /// Returns true if sucessfull. 
        /// Returns false if no Allocation exists with the given name.
        /// </returns>
        virtual public bool RemoveAllocation( String name )
        {
            if (this.allocations.ContainsKey( name ) == false)
            {
                return false;
            }

            this.unallocated += this.allocations[name].initAmt;
            this.allocated   -= this.allocations[name].initAmt;
            this.allocations.Remove( name );
            return true;
        }
        #endregion

        #region Backing Store
        private DateTime _fromDate;
        private DateTime _toDate;

        private decimal _moneyPool;
        private decimal _allocated;
        private decimal _unallocated;

        private Dictionary<String, Allocation> _allocations;
        #endregion
    }
}

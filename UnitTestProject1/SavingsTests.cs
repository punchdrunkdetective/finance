﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finance;

namespace UnitTests
{
    [TestClass]
    public class SavingsTests
    {
        // NOTE: It is assumed that the Savings' isValidWithdrawalIndex and isValidDepositIndex methods are implicitly tested by testing
        //       and update methods. If they did not work, simply everything would fail. (I really just don't want to write test cases for
        //       them though. 

        #region Sucessfull Construction
        [TestMethod]
        public void TestSucessfullDefaultConstruction()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            Assert.AreEqual( "testSavings",  testSavings.testName, "Savings name is incorrect at construction."          );
            Assert.AreEqual( true,           testSavings.testOpen, "Savings not automatically open at construction."     );
            Assert.AreEqual( 0.00m,          testSavings.currAmt,  "Savings balance not defaulted to 0 at construction." );
            Assert.AreEqual( DateTime.Today, testSavings.openDate, "Savings open date is incorrect at construction."     );

            Assert.IsNotNull( testSavings.testWithdrawals, "Withdrawals list not initialized at constrution." );
            Assert.IsNotNull( testSavings.testDeposits,    "Deposits list not initialized at construction."   );

            Assert.AreEqual( 0, testSavings.testWithdrawals.Count, "Withdrawals list not empty at construction." );
            Assert.AreEqual( 0, testSavings.testDeposits.Count,    "Deposits list not empty at construction."    );
        }

        [TestMethod]
        public void TestSucessfullConstructionWithInitialAmount()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today, 100.00m );

            Assert.AreEqual( "testSavings",  testSavings.testName, "Savings name is incorrect at construction."                     );
            Assert.AreEqual( true,           testSavings.testOpen, "Savings not automatically open at construction."                );
            Assert.AreEqual( 100.00m,        testSavings.currAmt,  "Savings balance not defaulted to amount given at construction." );
            Assert.AreEqual( DateTime.Today, testSavings.openDate, "Savings open date is incorrect at construction."                );

            Assert.IsNotNull( testSavings.testWithdrawals, "Withdrawals list not initialized at constrution." );
            Assert.IsNotNull( testSavings.testDeposits,    "Deposits list not initialized at construction."   );

            Assert.AreEqual( 0, testSavings.testWithdrawals.Count, "Withdrawals list not empty at construction."                          );
            Assert.AreEqual( 1, testSavings.testDeposits.Count,    "Deposits list is not set up with initial deposit at construction."    );

            Assert.AreEqual( "Initial deposit.", testSavings.testDeposits[0].descr, "Default description for initial deposit is incorrect." );
        }
        #endregion
        
        #region Sucessfull SetProperties
        [TestMethod]
        public void TestSucessfullSetName()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testName = "newSavings";

            Assert.AreEqual( "newSavings", testSavings.testName, "Savings name did not set." );
        }

        [TestMethod]
        public void TestSucessfullSetOpenToTrue()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testOpen = true;

            Assert.IsTrue( testSavings.testOpen, "Savings open state did not set to true." );
        }

        [TestMethod]
        public void TestSucessfullSetOpenToFalse()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testOpen = false;

            Assert.IsFalse( testSavings.testOpen, "Savings open state did not set to false." );
        }

        [TestMethod]
        public void TestSucessfullSetCurrAmt()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testCurrAmt = 100.00m;

            // NOTE: You are only testing that the setter works, not that the current amount will always have 
            //       a collection of Transactions associated with it.
            Assert.AreEqual( 100.00m, testSavings.testCurrAmt, "Savings current amount did not set." );
        }

        [TestMethod]
        public void TestSucessfullSetOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testOpenDate = DateTime.Today.AddDays( 2 );

            Assert.AreEqual( DateTime.Today.AddDays( 2 ), testSavings.testOpenDate, "Savings open date did not set." );
        }

        [TestMethod]
        public void TestSucessfullRemovalOfTransactionsWhenSettingOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            testSavings.MakeDeposit( DateTime.Today, "initial deposit", 500.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal1", 200.00m ); // Savings is currently at 300.00

            testSavings.MakeDeposit( DateTime.Today.AddDays( 1 ), "testDeposit1", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "testWithdrawal2", 25.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "testWithdrawal3", 25.00m ); // Savings is currently at 350.00

            int     newDepositCount    = testSavings.GetDeposits().Count    - 1;
            int     newWithdrawalCount = testSavings.GetWithdrawals().Count - 1;
            decimal newCurrAmt         = 50.00m;

            testSavings.testOpenDate = DateTime.Today.AddDays( 1 );

            Assert.AreEqual( newDepositCount,    testSavings.GetDeposits().Count,    "Deposits were not removed from list."    );
            Assert.AreEqual( newWithdrawalCount, testSavings.GetWithdrawals().Count, "Withdrawals were not removed from list." );
            Assert.AreEqual( newCurrAmt,         testSavings.currAmt,                "Savings' currAmt was not reduced."       );
        }
        #endregion

        #region Failure SetProperties
        [TestMethod]
        public void TestFailureToSetCurrAmtToNegative()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            ArgumentException negativeAmount = null;
            try
            {
                testSavings.testCurrAmt = -100.00m;
            }
            catch (ArgumentException e)
            {
                negativeAmount = e;
            }

            Assert.IsNotNull( negativeAmount, "Exception not thrown when setting current amount to negative value." );
            Assert.AreEqual( 0.00m, testSavings.testCurrAmt, "Current amount changed during failed set." );
        }

        [TestMethod]
        public void TestFailureOfRemovalOfTransactionsWhenSettingOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            testSavings.MakeDeposit( DateTime.Today, "initial deposit", 500.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal1", 200.00m ); // Savings is currently at 300.00

            testSavings.MakeDeposit( DateTime.Today.AddDays( 1 ), "testDeposit1", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "testWithdrawal2", 150.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "testWithdrawal3", 150.00m ); // Savings is currently at 100.00

            int     oldDepositCount    = testSavings.GetDeposits().Count;
            int     oldWithdrawalCount = testSavings.GetWithdrawals().Count ;
            decimal oldCurrAmt         = testSavings.currAmt; 

            ArgumentException invalidBalance = null;
            try
            {
                testSavings.testOpenDate = DateTime.Today.AddDays( 1 );
            }
            catch (ArgumentException e)
            {
                invalidBalance = e;
            }

            Assert.IsNotNull( invalidBalance, "Excetion was not thrown." );

            Assert.AreEqual( oldDepositCount,    testSavings.GetDeposits().Count,    "Deposits were removed from list."    );
            Assert.AreEqual( oldWithdrawalCount, testSavings.GetWithdrawals().Count, "Withdrawals were removed from list." );
            Assert.AreEqual( oldCurrAmt,         testSavings.currAmt,                "Savings' currAmt was reduced."       );
        }
        #endregion

        #region Sucessfull MakeWithdrawal
        [TestMethod]
        public void TestMakeWithdrawal()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "init deposit", 10.00m );

            DateTime date        = DateTime.Today;
            String   description = "testWithdrawal";
            decimal  amt         = 10.00m;

            this.TestSucessfullMakeWithdrawal( testSavings, date, description, amt );
        }
        #endregion

        #region Failure MakeWithdrawal
        [TestMethod]
        public void TestFailureToMakeNegativeWithdrawal()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "init deposit", 100.00m );

            DateTime date        = DateTime.Today;
            String   description = "testWithdrawal";
            decimal  amt         = -5.00m;

            this.TestFailureToMakeWithdrawal( testSavings, date, description, amt );
        }

        [TestMethod]
        public void TestFailureToMakeWithdrawalForClosedSavings()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "init deposit", 100.00m );
            testSavings.testOpen = false;

            DateTime date        = DateTime.Today;
            String   description = "testWithdrawal";
            decimal  amt         = 5.00m;

            this.TestFailureToMakeWithdrawal( testSavings, date, description, amt );
        }

        [TestMethod]
        public void TestFailureToMakeTooLargeOfAWithdrawal()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "init deposit", 100.00m );

            DateTime date        = DateTime.Today;
            String   description = "testWithdrawal";
            decimal  amt         = 110.00m;

            this.TestFailureToMakeWithdrawal( testSavings, date, description, amt );
        }

        [TestMethod]
        public void TestFailureToMakeWithdrawalBeforeOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "init deposit", 100.00m );

            DateTime date        = DateTime.Today.AddDays( -1 );
            String   description = "testWithdrawal";
            decimal  amt         = 10.00m;

            this.TestFailureToMakeWithdrawal( testSavings, date, description, amt );
        }
        #endregion

        #region Sucessfull MakeDeposit
        [TestMethod]
        public void TestMakeDeposit()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            DateTime date        = DateTime.Today;
            String   description = "testDeposit";
            decimal  amt         = 10.00m;

            this.TestSucessfullMakeDeposit( testSavings, date, description, amt );
        }
        #endregion

        #region Failure MakeDeposit
        [TestMethod]
        public void TestFailureToMakeDepositWithNegativeAmount()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            DateTime date        = DateTime.Today;
            String   description = "testDeposit";
            decimal  amt         = -10.00m;

            this.TestFailureToMakeDeposit( testSavings, date, description, amt );
        }

        [TestMethod]
        public void TestFailureToMakeDepositForClosedSavings()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.testOpen = false;

            DateTime date        = DateTime.Today;
            String   description = "testDeposit";
            decimal  amt         = 10.00m;

            this.TestFailureToMakeDeposit( testSavings, date, description, amt );
        }

        [TestMethod]
        public void TestFailureToMakeDepositBeforeOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );

            DateTime date        = DateTime.Today.AddDays( -1 );
            String   description = "testDeposit";
            decimal  amt         = 10.00m;

            this.TestFailureToMakeDeposit( testSavings, date, description, amt );
        }
        #endregion

        #region Sucessfull UpdateWithdrawal
        [TestMethod]
        public void TestSucessfullUpdateWithdrawalDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestSucessfullUpdateWithdrawalDate( testSavings, 0, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestSucessfullUpdateWithdrawalDeposit()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestSucessfullUpdateWithdrawalDescription( testSavings, 0, "updatedWithdrawal" );
        }

        [TestMethod]
        public void TestSucessfullUpdateWithdrawalAmount()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestSucessfullUpdateWithdrawalAmount( testSavings, 0, 20.00m );
        }
        #endregion

        #region Failure UpdateWithdrawal
        [TestMethod]
        public void TestFailureToUpdateWithdrawalDateWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalDate( testSavings, -1, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalDateWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalDate( testSavings, 1, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalDateBeforeOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalDate( testSavings, 0, DateTime.Today.AddDays( -1 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalDescriptionWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalDescription( testSavings, -1, "updatedWithdrawal" );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalDescriptionWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalDescription( testSavings, 1, "updatedWithdrawal" );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalAmountWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalAmount( testSavings, -1, 20.00m );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalAmountWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalAmount( testSavings, 1, 20.00m );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalAmountWithNegativeValue()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalAmount( testSavings, 0, -20.00m );
        }

        [TestMethod]
        public void TestFailureToUpdateWithdrawalAmountToOutOfBalanceSavings()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initial amount", 20.00m );
            testSavings.MakeWithdrawal( DateTime.Today, "testWithdrawal", 10.00m );

            this.TestFailureToUpdateWithdrawalAmount( testSavings, 0, 30.00m );       
        }
        #endregion

        #region Sucessfull UpdateDeposit
        [TestMethod]
        public void TestSucessfullUpdateDepositDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestSucessfullUpdateDepositDate( testSavings, 0, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestSucessfullUpdateDepositDescription()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestSucessfullUpdateDepositDescription( testSavings, 0, "updatedDeposit" );
        }

        [TestMethod]
        public void TestSucessfullUpdateDepositAmount()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestSucessfullUpdateDepositAmount( testSavings, 0, 20.00m );
        }
        #endregion

        #region Failure UpdateDeposit
        [TestMethod]
        public void TestFailureToUpdateDepositDateWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositDate( testSavings, -1, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositDateWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositDate( testSavings, 2, DateTime.Today.AddDays( 2 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositDateBeforeOpenDate()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositDate( testSavings, 0, DateTime.Today.AddDays( -1 ) );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositDescriptionWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositDescription( testSavings, -1, "updatedDeposit" );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositDescriptionWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositDescription( testSavings, 2, "updatedDeposit" );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositAmountWithNegativeValue()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositAmount( testSavings, 0, -10.00m );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositAmountWithIndexBeforeRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositAmount( testSavings, -1, 20.00m );
        }

        [TestMethod]
        public void TestFailureToUpdateDepositAmountWithIndexAfterRange()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "testDeposit", 10.00m );

            this.TestFailureToUpdateDepositAmount( testSavings, 2, 20.00m );
        }
        #endregion

        #region Sucessfull RemoveWithdrawal
        [TestMethod]
        public void TestSucessfullRemoveWithdrawal()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m ); // Savings current balance is 75.00 

            bool sucess = testSavings.RemoveWithdrawal( 0 );

            Assert.IsTrue( sucess, "RemoveWithdrawal returned false." );

            Assert.AreEqual( 0,       testSavings.GetWithdrawals().Count, "Withdrawal not removed from internal list."              );
            Assert.AreEqual( 100.00m, testSavings.currAmt,                "Savings current balance did not readjust after removal." );
        }
        #endregion

        #region Failure RemoveWithdrawal
        [TestMethod]
        public void TestFailureToRemoveWithdrawalBelowListBounds()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m ); // Savings current balance is 75.00 

            bool sucess = testSavings.RemoveWithdrawal( -1 );

            Assert.IsFalse( sucess, "RemoveWithdrawal returned true." );

            Assert.AreEqual( 1,      testSavings.GetWithdrawals().Count, "Withdrawal removed from internal list."              );
            Assert.AreEqual( 75.00m, testSavings.currAmt,                "Savings current balance did readjust after removal." );
        }

        [TestMethod]
        public void TestFailureToRemoveWithdrawalAfterListBounds()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m ); // Savings current balance is 75.00 

            bool sucess = testSavings.RemoveWithdrawal( 1 );

            Assert.IsFalse( sucess, "RemoveWithdrawal returned true." );

            Assert.AreEqual( 1,      testSavings.GetWithdrawals().Count, "Withdrawal removed from internal list."              );
            Assert.AreEqual( 75.00m, testSavings.currAmt,                "Savings current balance did readjust after removal." );
        }
        #endregion

        #region Sucesfull RemoveDeposit
        [TestMethod]
        public void TestSucessfullRemoveDeposit()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m ); 
            testSavings.MakeDeposit( DateTime.Today.AddDays( 2 ), "didn't need all that money", 10.00m ); // Savings current balance is 85.00 

            bool sucess = testSavings.RemoveDeposit( 1 );

            Assert.IsTrue( sucess, "RemoveDeposit returned false." );

            Assert.AreEqual( 1,      testSavings.GetDeposits().Count, "Deposit not removed from internal list."                 );
            Assert.AreEqual( 75.00m, testSavings.currAmt,             "Savings current balance did not readjust after removal." );
        }
        #endregion

        #region Failure RemoveDeposit
        [TestMethod]
        public void TestFailureToRemoveDepositBelowListBounds()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m );
            testSavings.MakeDeposit( DateTime.Today.AddDays( 2 ), "didn't need all that money", 10.00m ); // Savings current balance is 85.00 

            bool sucess = testSavings.RemoveDeposit( -1 );

            Assert.IsFalse( sucess, "RemoveDeposit returned true." );

            Assert.AreEqual( 2,      testSavings.GetDeposits().Count, "Deposit removed from internal list."                 );
            Assert.AreEqual( 85.00m, testSavings.currAmt,             "Savings current balance did readjust after removal." );
        }

        [TestMethod]
        public void TestFailureToRemoveDepositAfterListBounds()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m );
            testSavings.MakeDeposit( DateTime.Today.AddDays( 2 ), "didn't need all that money", 10.00m ); // Savings current balance is 85.00 

            bool sucess = testSavings.RemoveDeposit( 2 );

            Assert.IsFalse( sucess, "RemoveDeposit returned true." );

            Assert.AreEqual( 2,      testSavings.GetDeposits().Count, "Deposit removed from internal list."                 );
            Assert.AreEqual( 85.00m, testSavings.currAmt,             "Savings current balance did readjust after removal." );
        }

        [TestMethod]
        public void TestFailureToPutSavingsInNegativeBalanceByRemovingDeposit()
        {
            ExposedSavings testSavings = new ExposedSavings( "testSavings", DateTime.Today );
            testSavings.MakeDeposit( DateTime.Today, "initialDeposit", 100.00m );
            testSavings.MakeWithdrawal( DateTime.Today.AddDays( 1 ), "stuff", 25.00m ); // Savings current balance is 75.00 

            bool sucess = testSavings.RemoveDeposit( 0 );

            Assert.IsFalse( sucess, "RemoveDeposit returned true." );

            Assert.AreEqual( 1,      testSavings.GetDeposits().Count, "Deposit removed from internal list."                 );
            Assert.AreEqual( 75.00m, testSavings.currAmt,             "Savings current balance did readjust after removal." );
        }
        #endregion

        #region Utilities
        /// <summary>
        /// A utility method to test the sucessfull making of an withdrawal.
        /// </summary>
        public void TestSucessfullMakeWithdrawal( Savings testSavings, DateTime date, String description, decimal amt )
        {
            decimal newCurrAmt         = testSavings.currAmt - amt;
            int     newWithdrawalCount = testSavings.GetWithdrawals().Count + 1;

            bool withdrawalMade = testSavings.MakeWithdrawal( date, description, amt );
            Assert.IsTrue( withdrawalMade, "A withdrawal was not sucessfully made." );

            Assert.AreEqual( newCurrAmt,         testSavings.currAmt,                "Savings currAmt was not decreased correctly."                  );
            Assert.AreEqual( newWithdrawalCount, testSavings.GetWithdrawals().Count, "Transaction was added to the list of withdrawals incorrectly." );
        }

        /// <summary>
        /// A utility method to test the unsucessfull making of an withdrawal.
        /// </summary>
        public void TestFailureToMakeWithdrawal( Savings testSavings, DateTime date, String description, decimal amt )
        {
            decimal oldCurrAmt         = testSavings.currAmt;
            int     oldWithdrawalCount = testSavings.GetWithdrawals().Count;

            bool withdrawalMade = testSavings.MakeWithdrawal( date, description, amt );
            Assert.IsFalse( withdrawalMade, "A withdrawal was sucessfully made." );

            Assert.AreEqual( oldCurrAmt,         testSavings.currAmt,                "Savings currAmt was not decreased correctly."                  );
            Assert.AreEqual( oldWithdrawalCount, testSavings.GetWithdrawals().Count, "Transaction was added to the list of withdrawals incorrectly." );
        }

        /// <summary>
        /// A utility method to test the sucessfull making of an deposit.
        /// </summary>
        public void TestSucessfullMakeDeposit( Savings testSavings, DateTime date, String description, decimal amt )
        {
            decimal newCurrAmt      = testSavings.currAmt             + amt;
            int     newDepositCount = testSavings.GetDeposits().Count + 1;

            bool depositMade = testSavings.MakeDeposit( date, description, amt );
            Assert.IsTrue( depositMade, "A deposit was not sucessfully made." );

            Assert.AreEqual( newCurrAmt,      testSavings.currAmt,             "Savings currAmt was not increased."                 );
            Assert.AreEqual( newDepositCount, testSavings.GetDeposits().Count, "Deposit was not added to Savings list of deposits." );
        }

        /// <summary>
        /// A utility method to test the unsucessfull making of an deposit.
        /// </summary>
        public void TestFailureToMakeDeposit( Savings testSavings, DateTime date, String description, decimal amt )
        {
            decimal oldCurrAmt      = testSavings.currAmt;
            int     oldDepositCount = testSavings.GetDeposits().Count;

            bool depositMade = testSavings.MakeDeposit( date, description, amt );
            Assert.IsFalse( depositMade, "Made deposit was made." );

            Assert.AreEqual( oldCurrAmt,      testSavings.currAmt,             "Transaction was added to list of deposits." );
            Assert.AreEqual( oldDepositCount, testSavings.GetDeposits().Count, "Transaction was added to list of deposits." );
        }

        /// <summary>
        /// Test the sucessfull update of a withdrawal date. 
        /// </summary>
        public void TestSucessfullUpdateWithdrawalDate( Savings testSavings, int index, DateTime newDate )
        {
            bool sucess = testSavings.UpdateWithdrawal( index, newDate );
            Assert.IsTrue( sucess, "UpdateWithdrawal returned false." );
            Assert.AreEqual( newDate, testSavings.GetWithdrawals()[index].date, "Date did not sucessfully update." );
        }

        /// <summary>
        /// Test the sucessfull update of a withdrawal description. 
        /// </summary>
        public void TestSucessfullUpdateWithdrawalDescription( Savings testSavings, int index, String newDescr )
        {
            bool sucess = testSavings.UpdateWithdrawal( index, newDescr );
            Assert.IsTrue( sucess, "UpdateWithdrawal returned false." );
            Assert.AreEqual( newDescr, testSavings.GetWithdrawals()[index].descr, "Description did not sucessfully update." );
        }

        /// <summary>
        /// Test the sucessfull update of a withdrawal amount. 
        /// </summary>
        public void TestSucessfullUpdateWithdrawalAmount( Savings testSavings, int index, decimal newAmt )
        {
            decimal newBalance = testSavings.currAmt;
            if (newAmt > testSavings.GetWithdrawals()[index].amt)
            {
                newBalance -= newAmt - testSavings.GetWithdrawals()[index].amt;
            }
            else if (newAmt < testSavings.GetWithdrawals()[index].amt)
            {
                newBalance += testSavings.GetWithdrawals()[index].amt - newAmt;
            }

            bool sucess = testSavings.UpdateWithdrawal( index, newAmt );
            Assert.IsTrue( sucess, "UpdateWithdrawal returned false." );

            Assert.AreEqual( newAmt, testSavings.GetWithdrawals()[index].amt, "Amount did not sucessfully update." );
            Assert.AreEqual( newBalance, testSavings.currAmt, "Savings balance did not update correctly." );
        }

        /// <summary>
        /// A utility method to test the failure to update a withdrawal's date.
        /// </summary>
        public void TestFailureToUpdateWithdrawalDate( Savings testSavings, int index, DateTime newDate )
        {
            DateTime oldDate = DateTime.MinValue; // NOTE: Only used if index is valid.
            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                oldDate = testSavings.GetWithdrawals()[index].date;
            }

            bool success = testSavings.UpdateWithdrawal( index, newDate );
            Assert.IsFalse( success, "UpdateWithdrawal returned true." );

            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                Assert.AreEqual( oldDate, testSavings.GetWithdrawals()[index].date, "Date was not updated correctly." );
            }
        }

        /// <summary>
        /// A utility method to test the failure to update a withdrawal's description.
        /// </summary>
        public void TestFailureToUpdateWithdrawalDescription( Savings testSavings, int index, String newDescr )
        {
            String oldDescr = ""; // NOTE: Only used if index is valid.
            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                oldDescr = testSavings.GetWithdrawals()[index].descr;
            }

            bool success = testSavings.UpdateWithdrawal( index, newDescr );
            Assert.IsFalse( success, "UpdateWithdrawal returned true." );

            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                Assert.AreEqual( oldDescr, testSavings.GetWithdrawals()[index].descr, "Description was not updated correctly." );
            }
        }

        /// <summary>
        /// A utility method to test the failure to update a withdrawal's amount.
        /// </summary>
        public void TestFailureToUpdateWithdrawalAmount( Savings testSavings, int index, decimal newAmt )
        {
            decimal oldBalance = testSavings.currAmt;
            decimal oldAmt     = 0.00m; // NOTE: Only used if index is valid.
            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                oldAmt = testSavings.GetWithdrawals()[index].amt;
            }

            bool success = testSavings.UpdateWithdrawal( index, newAmt );
            Assert.IsFalse( success, "UpdateWithdrawal returned true." );
            Assert.AreEqual( oldBalance, testSavings.currAmt, "Savings balance updated." );

            if (0 <= index && index < testSavings.GetWithdrawals().Count)
            {
                Assert.AreEqual( oldAmt, testSavings.GetWithdrawals()[index].amt, "Amount was not updated correctly." );
            }
        }

        /// <summary>
        /// A utility method to test the sucessfull update of a deposit date.
        /// </summary>
        public void TestSucessfullUpdateDepositDate( Savings testSavings, int depositUnderTest, DateTime newDate )
        {
            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newDate );
            Assert.IsTrue( sucess, "UpdateDescription returned false." );
            Assert.AreEqual( newDate, testSavings.GetDeposits()[depositUnderTest].date, "Date did not sucessfully update." );
        }

        /// <summary>
        /// A utility method to test the unsucessfull update of a deposit date.
        /// </summary>
        public void TestSucessfullUpdateDepositDescription( Savings testSavings, int depositUnderTest, String newDescr )
        {
            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newDescr );
            Assert.IsTrue( sucess, "UpdateDescription returned false." );
            Assert.AreEqual( newDescr, testSavings.GetDeposits()[depositUnderTest].descr, "Description did not sucessfully update." );
        }

        /// <summary>
        /// A utility method to test the sucessfull update of a deposit description.
        /// </summary>
        public void TestSucessfullUpdateDepositAmount( Savings testSavings, int depositUnderTest, decimal newAmt )
        {
            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newAmt );
            Assert.IsTrue( sucess, "UpdateDescription returned false." );
            Assert.AreEqual( newAmt, testSavings.GetDeposits()[depositUnderTest].amt, "Amount did not sucessfully update." );
        }

        /// <summary>
        /// A utility method to test the unsucessfull update of a deposit description.s
        /// </summary>
        public void TestFailureToUpdateDepositDate( Savings testSavings, int depositUnderTest, DateTime newDate )
        {
            DateTime oldDate = DateTime.MinValue; // NOTE: Only used if there is truelly an old value.
            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                oldDate = testSavings.GetDeposits()[depositUnderTest].date;
            }

            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newDate );
            Assert.IsFalse( sucess, "UpdateDescription returned true." );

            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                Assert.AreEqual( oldDate, testSavings.GetDeposits()[depositUnderTest].date, "Date sucessfully update." );
            }
        }

        /// <summary>
        /// A utility method to test the sucessfull update of a deposit amount.
        /// </summary>
        public void TestFailureToUpdateDepositDescription( Savings testSavings, int depositUnderTest, String newDescr )
        {
            String oldDescr = ""; // NOTE: Only used if there is truelly an old value.
            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                oldDescr = testSavings.GetDeposits()[depositUnderTest].descr;
            }

            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newDescr );
            Assert.IsFalse( sucess, "UpdateDescription returned true." );

            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                Assert.AreEqual( oldDescr, testSavings.GetDeposits()[depositUnderTest].descr, "Description sucessfully update." );
            }
        }

        /// <summary>
        /// A utility method to test the unsucessfull update of a deposit amount.
        /// </summary>
        public void TestFailureToUpdateDepositAmount( Savings testSavings, int depositUnderTest, decimal newAmt )
        {
            decimal oldAmt = 0.00m; // NOTE: Only used if there is truelly an old value.
            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                oldAmt = testSavings.GetDeposits()[depositUnderTest].amt;
            }

            bool sucess = testSavings.UpdateDeposit( depositUnderTest, newAmt );
            Assert.IsFalse( sucess, "UpdateDescription returned true." );

            if (0 <= depositUnderTest && depositUnderTest < testSavings.GetDeposits().Count)
            {
                Assert.AreEqual( oldAmt, testSavings.GetDeposits()[depositUnderTest].amt, "Amount sucessfully update." );
            }
        }

        ///<summary>
        /// A test utility class that exposes hidden elements of an Savings. This is especially needed since Savings's only constructor is internal. 
        /// The convention used is to use this class primary as a means of construction. Only access any exposed properties or methods while testing the
        /// construcion of an Savings or while testing the setters and getters of any hidden properties. Other than that, the normal public interface
        /// of Savings should be used for testing to avoid any false positives that might incure with manipulation of hidden state. 
        /// </summary>
        public class ExposedSavings : Savings
        {
            public ExposedSavings( String name, DateTime openDate )
                : base( name, openDate )
            {
                // do nothing
            }

            public ExposedSavings( String name, DateTime openDate, decimal initAmt )
                : base( name, openDate, initAmt )
            {
                // do nothing.
            }

            public String testName
            {
                get { return base.name;  }
                set { base.name = value; }
            }

            public bool testOpen
            {
                get { return base.open;  }
                set { base.open = value; }
            }

            public decimal testCurrAmt
            {
                get { return base.currAmt;  }
                set { base.currAmt = value; }
            }

            public DateTime testOpenDate
            {
                get { return base.openDate;  }
                set { base.openDate = value; }
            }

            public List<Transaction> testWithdrawals
            {
                get { return base.withdrawals; }
            }

            public List<Transaction> testDeposits
            {
                get { return base.deposits; }
            }
        }
        #endregion
    }
}

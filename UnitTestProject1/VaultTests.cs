﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finance;

namespace UnitTestProject1
{
    [TestClass]
    public class VaultTests
    {
        #region Sucessfull Construction
        [TestMethod]
        public void TestConstruction()
        {
            ExposedVault testVault = new ExposedVault();

            Assert.IsNotNull( testVault.testSavings, "Backing store did not get initialized." );
            Assert.AreEqual( 0, testVault.testSavings.Count, "Backing store is not empty upon construction." );
        }
        #endregion

        #region Sucessfull OpenSavings
        [TestMethod]
        public void TestSucessfullOpenNewSavings()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.OpenSavings( "testSavings", DateTime.Today );
            Assert.IsTrue( sucess, "OpenSavings returned false." );

            Dictionary<String, Savings> savings = testVault.GetSavings();
            Assert.AreEqual( 1, savings.Count, "Savings was not added to internal collection." );

            Assert.IsTrue( savings.ContainsKey( "testSavings" ), "New Savings not found in internal collection." );

            Savings newSavings = savings["testSavings"];
            Assert.AreEqual( "testSavings",  newSavings.name,     "New Savings not intialized with the correct name." );
            Assert.AreEqual( DateTime.Today, newSavings.openDate, "New Savings not intialized with the correct date." );
        }

        [TestMethod]
        public void TestSucessfullOpenOldSavings()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.OpenSavings( "testSavings" );
            Assert.IsTrue( sucess, "OpenSavings returned false." );

            Savings oldSavings = testVault.GetSavings()["testSavings"];
            Assert.IsTrue( oldSavings.open, "Old Savings was not reopened." );
        }
        #endregion

        #region Failure OpenSavings
        [TestMethod]
        public void TestFailureToOpenNewSavingWithNonUniqueName()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.OpenSavings( "testSavings", DateTime.Today );
            Assert.IsFalse( sucess, "OpenSavings returned true." );

            Dictionary<String, Savings> savings = testVault.GetSavings();
            Assert.AreEqual( 1, savings.Count, "Savings was added to internal collection." );
        }

        [TestMethod]
        public void TestFailureToOpenNonExistantOldSaving()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.OpenSavings( "testSavings" );
            Assert.IsFalse( sucess, "OpenSavings returned true." );
        }

        [TestMethod]
        public void TestFailureToOpenOldSavingAlreadyOpened()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.OpenSavings( "testSavings" );
            Assert.IsFalse( sucess, "OpenSavings returned true." );

            Savings oldSavings = testVault.GetSavings()["testSavings"];
            Assert.IsTrue( oldSavings.open, "Old Savings closed unexpectedly." );
        }
        #endregion

        #region Sucessfull CloseSavings
        [TestMethod]
        public void TestSucessfullCloseSavings()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.CloseSavings( "testSavings" );
            Assert.IsTrue( sucess, "CloseSavings returned false." );

            Savings changedSavings = testVault.GetSavings()["testSavings"];
            Assert.IsFalse( changedSavings.open, "Specified Savings was not closed." );
        }
        #endregion

        #region Failure CloseSavings
        [TestMethod]
        public void TestFailureToCloseSavingsThatDoesntExist()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.CloseSavings( "testSavings" );
            Assert.IsFalse( sucess, "CloseSavings returned true." );
        }

        [TestMethod]
        public void TestFailureToCloseSavingsAlreadyClosed()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.CloseSavings( "testSavings" );
            Assert.IsFalse( sucess, "CloseSavings returned true." );

            Savings unChangedSavings = testVault.GetSavings()["testSavings"];
            Assert.IsFalse( unChangedSavings.open, "Specified Savings is no longer closed." );
        }
        #endregion

        #region Sucessfull RemoveSavings
        [TestMethod]
        public void TestSucessfullRemoveOpenSavings()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.RemoveSavings( "testSavings" );
            Assert.IsTrue( sucess, "RemoveSavings returned false." );

            Assert.AreEqual( 0, testVault.GetSavings().Count, "Savings collection is not empty." );
        }

        [TestMethod]
        public void TestSucessfullRemoveClosedSavings()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.RemoveSavings( "testSavings" );
            Assert.IsTrue( sucess, "RemoveSavings returned false." );

            Assert.AreEqual( 0, testVault.GetSavings().Count, "Savings collection is not empty." );
        }
        #endregion

        #region Failure RemoveSavings
        [TestMethod]
        public void TestFailureToRemoveSavingsThatDoesntExist()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.RemoveSavings( "testSavings" );
            Assert.IsFalse( sucess, "RemoveSavings returned true." );
        }
        #endregion

        #region Sucessfull UpdateSavings
        [TestMethod]
        public void TestSucessfullUpdateOpenSavingsName()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.UpdateSavings( "testSavings", "updatedTestSavings" );
            Assert.IsTrue( sucess, "UpdateSavings return false." );

            Assert.IsTrue( testVault.GetSavings().ContainsKey( "updatedTestSavings" ), "Internal list doesn't contatin updated key." );
            Assert.AreEqual( "updatedTestSavings", testVault.GetSavings()["updatedTestSavings"].name, "Actual Savings name wasn't updated." );
        }

        [TestMethod]
        public void TestSucessfullUpdateClosedSavingsName()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.UpdateSavings( "testSavings", "updatedTestSavings" );
            Assert.IsTrue( sucess, "UpdateSavings return false." );

            Assert.IsTrue( testVault.GetSavings().ContainsKey( "updatedTestSavings" ), "Internal list doesn't contatin updated key." );
            Assert.AreEqual( "updatedTestSavings", testVault.GetSavings()["updatedTestSavings"].name, "Actual Savings name wasn't updated." );
        }

        [TestMethod]
        public void TestSucessfullUpdateOpenSavingsOpenDate()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.UpdateSavings( "testSavings", DateTime.Today.AddDays( 1 ) );
            Assert.IsTrue( sucess, "UpdateSavings return false." );

            Assert.AreEqual( DateTime.Today.AddDays( 1 ), testVault.GetSavings()["testSavings"].openDate, "Actual Savings openDate wasn't updated." );
        }

        [TestMethod]
        public void TestSucessfullUpdateClosedSavingsOpenDate()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.UpdateSavings( "testSavings", DateTime.Today.AddDays( 1 ) );
            Assert.IsTrue( sucess, "UpdateSavings return false." );

            Assert.AreEqual( DateTime.Today.AddDays( 1 ), testVault.GetSavings()["testSavings"].openDate, "Actual Savings openDate wasn't updated." );
        }
        #endregion

        #region Failure UpdateSavings
        [TestMethod]
        public void TestFailureToUpdateNonExistantSavingsName()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.UpdateSavings( "testSavings", "updatedTestSavings" );
            Assert.IsFalse( sucess, "UpdateSavings return true." );
        }

        [TestMethod]
        public void TestFailureToUpdateOpenSavingsNameToNonUniqueName()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings1", DateTime.Today );
            testVault.OpenSavings( "testSavings2", DateTime.Today );

            bool sucess = testVault.UpdateSavings( "testSavings1", "testSavings2" );
            Assert.IsFalse( sucess, "UpdateSavings return true." );

            Assert.IsTrue( testVault.GetSavings().ContainsKey( "testSavings1" ), "Internal collection key was changed." );
            Assert.AreEqual( "testSavings1", testVault.GetSavings()["testSavings1"].name, "Savings name actually updated." );
        }

        [TestMethod]
        public void TestFailureToUpdateClosedSavingsNameToNonUniqueName()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings1", DateTime.Today );
            testVault.OpenSavings( "testSavings2", DateTime.Today );
            testVault.CloseSavings( "testSavings1" );

            bool sucess = testVault.UpdateSavings( "testSavings1", "testSavings2" );
            Assert.IsFalse( sucess, "UpdateSavings return true." );

            Assert.IsTrue( testVault.GetSavings().ContainsKey( "testSavings1" ), "Internal collection key was changed." );
            Assert.AreEqual( "testSavings1", testVault.GetSavings()["testSavings1"].name, "Savings name actually updated." );
        }

        [TestMethod]
        public void TestFailureToUpdateNonExistantSavingsOpenDate()
        {
            ExposedVault testVault = new ExposedVault();

            bool sucess = testVault.UpdateSavings( "testSavings", DateTime.Today );
            Assert.IsFalse( sucess, "UpdateSavings return true." );
        }

        [TestMethod]
        public void TestFailureToUpdateOpenSavingsOpenDateToSameDate()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );

            bool sucess = testVault.UpdateSavings( "testSavings", DateTime.Today );
            Assert.IsFalse( sucess, "UpdateSavings return true." );
        }

        [TestMethod]
        public void TestFailureToUpdateClosedSavingsOpenDateToSameDate()
        {
            ExposedVault testVault = new ExposedVault();
            testVault.OpenSavings( "testSavings", DateTime.Today );
            testVault.CloseSavings( "testSavings" );

            bool sucess = testVault.UpdateSavings( "testSavings", DateTime.Today );
            Assert.IsFalse( sucess, "UpdateSavings return true." );
        }
        #endregion

        #region Utilities
        /// <summary>
        /// A test utility class that exposes hidden elements of Vault. The convention used is to use this class primary as a means of construction.
        /// Only access any exposed properties or methods while testing the construcion of Vault or while testing the setters and getters of any 
        /// hidden properties. Other than that, the normal public interface of Vault should be used for testing to avoid any false positives that 
        /// might incure with manipulation of hidden state. 
        /// </summary>
        private class ExposedVault : Vault
        {
            public ExposedVault()
                : base()
            {
                // do nothing.
            }

            public Dictionary<String, Savings> testSavings
            {
                get { return base.savings; }
            }
        }
        #endregion
    }
}

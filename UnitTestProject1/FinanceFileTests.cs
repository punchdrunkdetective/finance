﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finance;

namespace UnitTests
{
    [TestClass]
    public class FinanceFileTests
    {
        #region Sucessfull Construction
        [TestMethod]
        public void TestConstruction()
        {
            ExposedFinanceFile testFile           = null;
            ArgumentException  failedConstruction = null;
            try
            {
                testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            }
            catch (ArgumentException e)
            {
                failedConstruction = e;
            }

            Assert.IsNull( failedConstruction, "FinanceFile threw exception during construction." );
            Assert.IsNotNull( testFile.testAllocations, "FinanceFile's allocation collection was not inialized." );

            Assert.AreEqual( DateTime.Today,              testFile.testFromDate, "FinanceFile's from date is not set correctly." );
            Assert.AreEqual( DateTime.Today.AddDays( 1 ), testFile.testToDate,   "FinanceFile's to date is not set correctly."   );

            Assert.AreEqual( 1000.00m, testFile.testMoneyPool,         "FinanceFile's money pool ammount was incorrectly set." );
            Assert.AreEqual( 0.00m,    testFile.testAllocated,         "FinanceFile's allocated ammount was incorrectly set."  );
            Assert.AreEqual( 1000.00m, testFile.testUnallocated,       "FinanceFile's unallocated amount was incorrectly set." );
            Assert.AreEqual( 0.00m,    testFile.testTotalSpent,        "FinanceFile's total spent amount was incorrectly set." );
            Assert.AreEqual( 1000.00m, testFile.testBalance,           "FinanceFile's balance was incorrectly set."            );
            Assert.AreEqual( 0,        testFile.testAllocations.Count, "FinanceFile's allocation list was incorrectly made."   );
        }
        #endregion

        #region Failure Construction
        [TestMethod]
        public void TestFailureToConstructWithInvalidDateRange()
        {
            ExposedFinanceFile testFile         = null;
            ArgumentException  invalidDateRange = null;
            try
            {
                testFile = new ExposedFinanceFile( DateTime.Today.AddDays( 1 ), DateTime.Today, 1000.00m );
            }
            catch (ArgumentException e)
            {
                invalidDateRange = e;
            }

            Assert.IsNotNull( invalidDateRange, "FinanceFile was sucessfully constructed." );
        }

        [TestMethod]
        public void TestFailureToConstructWithNegativeInitAmt()
        {
            ExposedFinanceFile testFile       = null;
            ArgumentException  negativeAmount = null;
            try
            {
                testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), -1000.00m );
            }
            catch (ArgumentException e)
            {
                negativeAmount = e;
            }

            Assert.IsNotNull( negativeAmount, "FinanceFile was sucessfully constructed." );
        }
        #endregion

        #region Sucessfull Set Properties
        [TestMethod]
        public void TestSucessfullSetFromDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testFromDate = DateTime.Today.AddDays( 1 );

            // NOTE: No side effects are anticipated.
            Assert.AreEqual( DateTime.Today.AddDays( 1 ), testFile.testFromDate, "From date didn't set." );
        }

        [TestMethod]
        public void TestSucessfullSetToDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testToDate = DateTime.Today;

            // NOTE: No side effects are anticipated.
            Assert.AreEqual( DateTime.Today, testFile.testToDate, "To date didn't set." );
        }

        [TestMethod]
        public void TestSucessfullSetMoneyPoolToGreaterValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testMoneyPool = 2000.00m;

            Assert.AreEqual( 2000.00m, testFile.testMoneyPool,   "Money pool didn't increase."         );
            Assert.AreEqual( 2000.00m, testFile.testUnallocated, "Unallocated amount didn't increase." );
        }

        [TestMethod]
        public void TestSucessfullSetMoneyPoolToLesserValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testMoneyPool = 500.00m;

            Assert.AreEqual( 500.00m, testFile.testMoneyPool,   "Money pool didn't decrease."         );
            Assert.AreEqual( 500.00m, testFile.testUnallocated, "Unallocated amount didn't decrease." );
        }

        [TestMethod]
        public void TestSucessfullSetAllocated()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testAllocated = 500.00m;

            // NOTE: No side effects are anticipated.
            Assert.AreEqual( 500.00m, testFile.testAllocated, "Allocated amount didn't set." );
        }

        [TestMethod]
        public void TestSucessfullSetUnallocated()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.testUnallocated = 500.00m;

            // NOTE: No side effects are anticipated.
            Assert.AreEqual( 500.00m, testFile.testUnallocated, "Allocated amount didn't set." );
        }

        [TestMethod]
        public void TestSucessfullUpdateOfAllocationsWhenSettingFromDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );
            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.MakeAllocation( "testAlloc3", 300.00m );

            testFile.testFromDate = testFile.testFromDate.AddDays( 1 );

            foreach (KeyValuePair<String, Allocation> tuple in testFile.GetAllocations())
            {
                Assert.AreEqual( DateTime.Today.AddDays( 1 ), tuple.Value.fromDate, "An Allocation did not update." );
            }
        }

        [TestMethod]
        public void TestSucessfullUpdateOfAllocationsWhenSettingToDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );
            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.MakeAllocation( "testAlloc3", 300.00m );

            testFile.testToDate = testFile.testToDate.AddDays( -1 );

            foreach (KeyValuePair<String, Allocation> tuple in testFile.GetAllocations())
            {
                Assert.AreEqual( DateTime.Today.AddDays( 1 ), tuple.Value.toDate,  "An Allocation did not update." );
            }
        }
        #endregion

        #region Failure Set Properties
        [TestMethod]
        public void TestFailureToSetFromDateAfterToDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException invalidDateRange = null;
            try
            {
                testFile.testFromDate = testFile.testToDate.AddDays( 1 );
            }
            catch (ArgumentException e)
            {
                invalidDateRange = e;
            }

            Assert.IsNotNull( invalidDateRange, "Exception not thrown." );
            Assert.AreEqual( DateTime.Today, testFile.testFromDate, "From date updated." );
        }

        [TestMethod]
        public void TestFailureToSetToDateBeforeFromDate()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException invalidDateRange = null;
            try
            {
                testFile.testToDate = testFile.testFromDate.AddDays( -1 );
            }
            catch (ArgumentException e)
            {
                invalidDateRange = e;
            }

            Assert.IsNotNull( invalidDateRange, "Exception not thrown." );
            Assert.AreEqual( DateTime.Today.AddDays( 1 ), testFile.testToDate, "To date updated." );
        }

        [TestMethod]
        public void TestFailureToSetMoneyPoolToNegativeValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException negativeValue = null;
            try
            {
                testFile.moneyPool = -1000.00m;
            }
            catch (ArgumentException e)
            {
                negativeValue = e;
            }

            Assert.IsNotNull( negativeValue, "Exception not thrown." );
            Assert.AreEqual( 1000.00m, testFile.testMoneyPool,   "Money pool value changed."  );
            Assert.AreEqual( 1000.00m, testFile.testUnallocated, "Unallocated value changed." );
        }

        [TestMethod]
        public void TestFailureToSetMoneyPoolToInValidateUnallocationValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );
            testFile.MakeAllocation( "testAlloc", 975.00m );

            ArgumentException invalidUnallocation = null;
            try
            {
                testFile.testMoneyPool = 974.99m;
            }
            catch (ArgumentException e)
            {
                invalidUnallocation = e;
            }

            Assert.IsNotNull( invalidUnallocation, "Exception not thrown." );
            Assert.AreEqual( 1000.00m, testFile.testMoneyPool,   "Unallocated value set."     );
            Assert.AreEqual( 25.00m,   testFile.testUnallocated, "Unallocated value changed." );
        }

        [TestMethod]
        public void TestFailureToSetAllocatedToNegativeValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException negativeValue = null;
            try
            {
                testFile.testAllocated = -100.00m;
            }
            catch (ArgumentException e)
            {
                negativeValue = e;
            }

            Assert.IsNotNull( negativeValue, "Exception not thrown." );
            Assert.AreEqual( 0.00m, testFile.testAllocated, "Allocated value changed." );
        }

        [TestMethod]
        public void TestFailureToSetAllocatedToValueGreaterThanMoneyPool()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException tooLargeValue = null;
            try
            {
                testFile.testAllocated = 1000.01m;
            }
            catch (ArgumentException e)
            {
                tooLargeValue = e;
            }

            Assert.IsNotNull( tooLargeValue, "Exception not thrown." );
            Assert.AreEqual( 0.00m, testFile.testAllocated, "Allocated value set." );
        }

        [TestMethod]
        public void TestFailureToSetUnallocatedToNegativeValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException negativeValue = null;
            try
            {
                testFile.testUnallocated = -100.00m;
            }
            catch (ArgumentException e)
            {
                negativeValue = e;
            }

            Assert.IsNotNull( negativeValue, "Exception not thrown." );
            Assert.AreEqual( 1000.00m, testFile.testUnallocated, "Unallocated value changed." );
        }

        [TestMethod]
        public void TestFailureToSetUnallocatedToValueGreaterThanMoneyPool()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 1000.00m );

            ArgumentException tooLargeValue = null;
            try
            {
                testFile.testUnallocated = 1000.01m;
            }
            catch (ArgumentException e)
            {
                tooLargeValue = e;
            }

            Assert.IsNotNull( tooLargeValue, "Exception not thrown." );
            Assert.AreEqual( 1000.00m, testFile.testUnallocated, "Unallocated value set." );
        }
        #endregion

        #region Aggregate Property Tests
        [TestMethod]
        public void TestSucessfullTotalSpencCalculation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );

            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense1", 10.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense2", 10.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense3", 10.00m );

            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense4", 20.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense5", 20.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense6", 20.00m );

            Assert.AreEqual( 90.00m, testFile.testTotalSpent, "Total Spent not being calculated correctly." );
        }

        [TestMethod]
        public void TestSucessfullBalanceCalculation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );

            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense1", 10.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense2", 10.00m );
            testFile.GetAllocations()["testAlloc1"].MakeExpense( DateTime.Today, "testExpense3", 10.00m );

            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense4", 20.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense5", 20.00m );
            testFile.GetAllocations()["testAlloc2"].MakeExpense( DateTime.Today, "testExpense6", 20.00m );

            Assert.AreEqual( 910.00m, testFile.testBalance, "Balance not being calculated correctly." );
        }
        #endregion

        #region Sucessfull MakeAllocation
        [TestMethod]
        public void TesSucessfulltMakeAllocation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );

            String  allocName = "testAlloc";
            decimal allocAmt  = 40.00m;

            this.TestSucessfullMakeAllocation( testFile, allocName, allocAmt );
        }
        #endregion

        #region Failure MakeAllocation
        [TestMethod]
        public void TestFailureToMakeAllocationWithDuplicateName()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String  allocName = "testAlloc";
            decimal allocAmt  = 40.00m;

            this.TestFailureToMakeAllocation( testFile, allocName, allocAmt );
        }

        [TestMethod]
        public void TestFailureToMakeAllocationWithTooLargeOfValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );

            String  allocName = "testAlloc";
            decimal allocAmt  = 100.01m;

            this.TestFailureToMakeAllocation( testFile, allocName, allocAmt );
        }

        [TestMethod]
        public void TestFailueToMakeAllocationWithNegativeValue()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );

            String  allocName = "testAlloc";
            decimal allocAmt  = -40.00m;

            this.TestFailureToMakeAllocation( testFile, allocName, allocAmt );
        }
        #endregion

        #region Sucessfull UpdateAllocation 
        [TestMethod]
        public void TesteSucessfullUpdateAllocationToMin()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );
            testFile.GetAllocations()["testAlloc"].MakeExpense( DateTime.Today, "testExpense", 10.00m );

            String  allocationUnderTest = "testAlloc";
            decimal tooAmt              = 0.00m;

            this.TestSucessfullUpdateOfAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestSucessfullUpdateAllocationToMax()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );
            testFile.GetAllocations()["testAlloc"].MakeExpense( DateTime.Today, "testExpense", 10.00m );

            String  allocationUnderTest = "testAlloc";
            decimal tooAmt              = 100.00m;

            this.TestSucessfullUpdateOfAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestSucessfullUpdateAllocationName()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String allocationUnderTest = "testAlloc";
            String newName             = "newTestAlloc";

            this.TestSucessfullUpdateOfAllocationName( testFile, allocationUnderTest, newName );
        }
        #endregion

        #region Failure UpdateAllocation
        [TestMethod]
        public void TestFailureToUpdateToFakeAllocationInitAmount()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );
            testFile.GetAllocations()["testAlloc"].MakeExpense( DateTime.Today, "testExpense", 10.00m );

            String  allocationUnderTest = "fakeAlloc";
            decimal tooAmt              = 40.00m;

            this.TestFailureToUpdateAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationToNegativeInitAmount()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String  allocationUnderTest = "testAlloc";
            decimal tooAmt              = -40.00m;

            this.TestFailureToUpdateAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationToNotNewInitAmount()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String  allocationUnderTest = "testAlloc";
            decimal tooAmt              = 40.00m;

            this.TestFailureToUpdateAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationInitAmountOverMoneyPool()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String  allocationUnderTest = "testAlloc";
            decimal tooAmt              = 1100.00m;

            this.TestFailureToUpdateAllocationInitAmt( testFile, allocationUnderTest, tooAmt );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationNameToNotNewName()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc", 40.00m );

            String allocationUnderTest = "testAlloc";
            String newName             = "testAlloc";

            this.TestFailedUpdateOfAllocationName( testFile, allocationUnderTest, newName );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationNameToNonUniqueName()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );
            testFile.MakeAllocation( "testAlloc1", 40.00m );
            testFile.MakeAllocation( "testAlloc2", 20.00m );

            String allocationUnderTest = "testAlloc1";
            String newName             = "testAlloc2";

            this.TestFailedUpdateOfAllocationName( testFile, allocationUnderTest, newName );
        }

        [TestMethod]
        public void TestFailureToUpdateAllocationNameOfNonExistantAllocation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 1 ), 100.00m );

            String allocationUnderTest = "testAlloc";
            String newName             = "newTestAlloc";

            this.TestFailedUpdateOfAllocationName( testFile, allocationUnderTest, newName );
        }
        #endregion

        #region Sucessfull RemoveAllocation
        [TestMethod]
        public void TestSucessfullRemoveAllocation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );
            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.MakeAllocation( "testAlloc3", 300.00m ); // Allocating a total of 600.00m

            bool sucess = testFile.RemoveAllocation( "testAlloc2" );

            Assert.IsTrue( sucess, "RemoveAllocation returned false." );

            Assert.AreEqual( 2,       testFile.GetAllocations().Count, "Allocation was not removed from Dictionary."       );
            Assert.AreEqual( 600.00m, testFile.unallocated,            "Unallocated amount is not being updated properly." );
            Assert.AreEqual( 400.00m, testFile.allocated,              "Allocated amount is not being updated properly."   );
        }
        #endregion

        #region Failure RemoveAllocation
        [TestMethod]
        public void TestFailureToRemoveAllocation()
        {
            ExposedFinanceFile testFile = new ExposedFinanceFile( DateTime.Today, DateTime.Today.AddDays( 2 ), 1000.00m );
            testFile.MakeAllocation( "testAlloc1", 100.00m );
            testFile.MakeAllocation( "testAlloc2", 200.00m );
            testFile.MakeAllocation( "testAlloc3", 300.00m ); // Allocating a total of 600.00m

            bool sucess = testFile.RemoveAllocation( "testAllocFake" );

            Assert.IsFalse( sucess, "RemoveAllocation returned true." );

            Assert.AreEqual( 3,       testFile.GetAllocations().Count, "Allocation was removed from Dictionary." );
            Assert.AreEqual( 400.00m, testFile.unallocated,            "Unallocated amount changed."             );
            Assert.AreEqual( 600.00m, testFile.allocated,              "Allocated amount changed."               );
        }
        #endregion

        #region Utilities
        /// <summary>
        /// A utility method to test the sucessfull making of an Allocation.
        /// </summary>
        public void TestSucessfullMakeAllocation( FinanceFile testFile, String allocName, decimal allocAmt )
        {
            decimal newFileMoneyPool   = testFile.moneyPool;
            decimal newFileAllocated   = testFile.allocated   + allocAmt;
            decimal newFileUnallocated = testFile.unallocated - allocAmt;
            decimal newFileTotalSpent  = testFile.totalSpent;
            decimal newFileBalance     = testFile.balance;
            decimal newAllocCount      = testFile.GetAllocations().Count + 1;

            bool sucess = testFile.MakeAllocation( allocName, allocAmt );

            Assert.IsTrue( sucess, "MakeAllocation returning false." );

            Assert.AreEqual( newFileMoneyPool,   testFile.moneyPool,              "FinanceFile moneyPool was not changed by allocation."     );
            Assert.AreEqual( newFileAllocated,   testFile.allocated,              "FinanceFile allocated was not increased by allocation."   );
            Assert.AreEqual( newFileUnallocated, testFile.unallocated,            "FinanceFile unallocated was not reduced by allocation."   );
            Assert.AreEqual( newFileTotalSpent,  testFile.totalSpent,             "FinanceFile totalSpent did not default to 0."             );
            Assert.AreEqual( newFileBalance,     testFile.balance,                "FinanceFile balance did not default to 0."                );
            Assert.AreEqual( newAllocCount,      testFile.GetAllocations().Count, "Allocation was not added to internal list of Allocations" );

            Allocation testAllocation = null;
            try
            {
                testAllocation = testFile.GetAllocations()[allocName];
            }
            catch (KeyNotFoundException)
            {
                Assert.Fail( "Allocation was not added to FinanceFile's list of Allocations." );
            }

            Assert.AreEqual( allocName, testAllocation.name,                "Allocation's name was incorrectly set."            );
            Assert.AreEqual( allocAmt,  testAllocation.initAmt,             "Allocation's initial ammount was incorrectly set." );
            Assert.AreEqual( allocAmt,  testAllocation.currAmt,             "Allocation's current amount was incorrectly set."  );
            Assert.AreEqual( 0,         testAllocation.GetExpenses().Count, "Allocation's expense list was incorrectly made."   );
        }

        /// <summary>
        /// A utility method to test the unsucessfull making of an Allocation.
        /// </summary>
        public void TestFailureToMakeAllocation( FinanceFile testFile, String allocName, decimal allocAmt )
        {
            decimal oldFileMoneyPool   = testFile.moneyPool;
            decimal oldFileAllocated   = testFile.allocated;
            decimal oldFileUnallocated = testFile.unallocated;
            decimal oldFileTotalSpent  = testFile.totalSpent;
            decimal oldFileBalance     = testFile.balance;
            decimal oldAllocCount      = testFile.GetAllocations().Count;

            bool allocAlreadyExists = testFile.GetAllocations().ContainsKey( allocName );

            bool sucess = testFile.MakeAllocation( allocName, allocAmt );
            Assert.IsFalse( sucess, "Allocation was made" );

            if (!allocAlreadyExists)
            {
                // Make sure it still doesn't exist.
                Assert.IsFalse( testFile.GetAllocations().ContainsKey( allocName ), "Allocation key was added to dictionary." );
            }

            Assert.AreEqual( oldFileMoneyPool,   testFile.moneyPool,   "FinanceFile moneyPool was changed during failed allocation."   );
            Assert.AreEqual( oldFileAllocated,   testFile.allocated,   "FinanceFile allocated was changed during failed allocation."   );
            Assert.AreEqual( oldFileUnallocated, testFile.unallocated, "FinanceFile unallocated was changed during failed allocation." );
            Assert.AreEqual( oldFileTotalSpent,  testFile.totalSpent,  "FinanceFile totalSpent was changed during failed allocation."  );
            Assert.AreEqual( oldFileBalance,     testFile.balance,     "FinanceFile balance was changed during failed allocation."     );

            Assert.AreEqual( oldAllocCount, testFile.GetAllocations().Count, "Failed allocation was added to internal list of Allocations" );
        }

        /// <summary>
        /// A utility method to test the sucessfull update of an Allocation's initial amount.
        /// </summary>
        public void TestSucessfullUpdateOfAllocationInitAmt( FinanceFile testFile, String allocationUnderTest, decimal tooAmt )
        {
            decimal amtDff = Math.Abs( testFile.GetAllocations()[allocationUnderTest].initAmt - tooAmt );

            // First, calculate the anticipated side effects. 
            decimal newUnallocated;
            decimal newAllocated;
            decimal newAllocCurrAmt;
            if (testFile.GetAllocations()[allocationUnderTest].initAmt < tooAmt)
            {
                // Increasing an Allocation's initial amount will decrease the FinanceFile's unallocated amount and increase the allocated amount.
                newUnallocated  = testFile.unallocated - amtDff;
                newAllocated    = testFile.allocated   + amtDff;
                newAllocCurrAmt = testFile.GetAllocations()[allocationUnderTest].currAmt + amtDff; // currAmt increases and decreases with initAmt
            }
            else if (testFile.GetAllocations()[allocationUnderTest].initAmt > tooAmt)
            {
                // Decreaseing an Allocation's initial amount will increase the FinanceFile's unallocated amount and decrease the allocated amount.
                newUnallocated  = testFile.unallocated + amtDff;
                newAllocated    = testFile.allocated   - amtDff;
                newAllocCurrAmt = testFile.GetAllocations()[allocationUnderTest].currAmt - amtDff; // currAmt increases and decreases with initAmt
            }
            else
            {
                newUnallocated  = testFile.unallocated;
                newAllocated    = testFile.allocated;
                newAllocCurrAmt = testFile.GetAllocations()[allocationUnderTest].currAmt;
            }            

            // Next, cause the side effects. 
            bool sucessfullAdjustment = testFile.UpdateAllocation( allocationUnderTest, tooAmt );
            Assert.IsTrue( sucessfullAdjustment, "UpdateAllocation returning false" );

            // Finally, test that the side effects actually occured. 
            Assert.AreEqual( newUnallocated, testFile.unallocated, "FinanceFile's unallocated did not change during update." );
            Assert.AreEqual( newAllocated,   testFile.allocated,   "FinanceFile's alllocated did not change during update."  );

            Assert.AreEqual( tooAmt,          testFile.GetAllocations()[allocationUnderTest].initAmt, "Test allocation initAmt incorrect." );
            Assert.AreEqual( newAllocCurrAmt, testFile.GetAllocations()[allocationUnderTest].currAmt, "Test allocation currAmt incorrect." );
        }

        /// <summary>
        /// A utility method to test the sucessfull update of an Allocation's initial amount.
        /// </summary>
        public void TestFailureToUpdateAllocationInitAmt( FinanceFile testFile, String allocationUnderTest, decimal tooAmt )
        {         
            // First, capture old state. This state is anticipated NOT to change.
            decimal oldUnallocated = testFile.unallocated;
            decimal oldAllocated   = testFile.allocated;

            // The following valuew will not be used unless the allocation under test actually exists.
            decimal oldAllocInitAmt = 0.00m;
            decimal oldAllocCurrAmt = 0.00m;
            if (testFile.GetAllocations().ContainsKey( allocationUnderTest ))
            {
                oldAllocInitAmt = testFile.GetAllocations()[allocationUnderTest].initAmt;
                oldAllocCurrAmt = testFile.GetAllocations()[allocationUnderTest].currAmt;
            }

            // Then, Attempt to change old state.
            bool sucessfullAdjustment = testFile.UpdateAllocation( allocationUnderTest, tooAmt );
            Assert.IsFalse( sucessfullAdjustment, "UpdateAllocation returning true" );

            // Finally, test that the old state did NOT change.
            Assert.AreEqual( oldUnallocated, testFile.unallocated, "FinanceFile unallocated changed during failed update." );
            Assert.AreEqual( oldAllocated,   testFile.allocated,   "FinanceFile allocated changed during failed update."   );

            if (testFile.GetAllocations().ContainsKey( allocationUnderTest ))
            {
                Assert.AreEqual( oldAllocInitAmt, testFile.GetAllocations()[allocationUnderTest].initAmt, "Test allocation initAmt changed during failed update." );
                Assert.AreEqual( oldAllocCurrAmt, testFile.GetAllocations()[allocationUnderTest].currAmt, "Test allocation currAmt changed during failed update." );
            }
        }

        /// <summary>
        /// A utility method to test the sucessfull update of an Allocation's name.
        /// </summary>
        public void TestSucessfullUpdateOfAllocationName( FinanceFile testFile, String allocationUnderTest, String newName )
        {
            bool nameChanged = testFile.UpdateAllocation( allocationUnderTest, newName );

            Assert.IsTrue( nameChanged, "UpdateAllocation returning false."                            );

            Assert.IsFalse( testFile.GetAllocations().ContainsKey( allocationUnderTest ), "Old Allocation key is not removed from FinaceFile's dictionary." );
            Assert.IsTrue( testFile.GetAllocations().ContainsKey( newName ), "Allocation's key is not updated in FinanceFile's dictionary." );

            Assert.IsNotNull( testFile.GetAllocations()[newName], "Allocation's key is not referencing the Allocation object in FinanceFile's dictionary." );

            Assert.AreEqual( newName, testFile.GetAllocations()[newName].name, "Allocation name was not updated." );
        }

        /// <summary>
        /// A utility method to test the failed update of an Allocation's name.
        /// </summary>
        public void TestFailedUpdateOfAllocationName( FinanceFile testFile, String allocationUnderTest, String newName )
        {
            bool newNameExisted = testFile.GetAllocations().ContainsKey( newName );

            bool nameChanged = testFile.UpdateAllocation( allocationUnderTest, newName );
            Assert.IsFalse( nameChanged, "UpdateAllocation returning true." );

            if (newNameExisted == false)
            {
                // Make sure that it still doesn't exist.
                Assert.IsFalse( testFile.GetAllocations().ContainsKey( newName ), "Allocation's key updated in FinanceFile's dictionary." );
            }
            else
            {
                // Make sure that it still does exist.
                Assert.IsTrue( testFile.GetAllocations().ContainsKey( newName ), "Allocation's key updated in FinanceFile's dictionary." );
            }

            if (testFile.GetAllocations().ContainsKey( allocationUnderTest ))
            {
                Assert.IsTrue( testFile.GetAllocations().ContainsKey( allocationUnderTest ), "Allocation's key no longer exists in FinanceFile's dictionary." );
                Assert.IsNotNull( testFile.GetAllocations()[allocationUnderTest], "Allocation's key is not referencing the Allocation object in FinanceFile's dictionary." );

                Assert.AreEqual( allocationUnderTest, testFile.GetAllocations()[allocationUnderTest].name, "Allocation name was updated." );
            }
        }
        
        /// <summary>
        /// A test utility class that exposes hidden elements of an FinanceFile. The convention used is to use this class primarly as a means of construction.
        /// Only access any exposed properties or methods while testing the construcion of an FinanceFile or while testing the setters and getters of any 
        /// hidden properties. Other than that, the normal public interface of FinanceFile should be used for testing to avoid any false positives that might
        /// incure with manipulation of hidden state. 
        /// </summary>
        private class ExposedFinanceFile : FinanceFile
        {
            public ExposedFinanceFile( DateTime fromDate, DateTime toDate, decimal initAmt )
                : base( fromDate, toDate, initAmt )
            {
                // do nothing.
            }

            public DateTime testFromDate
            {
                get { return base.fromDate;  }
                set { base.fromDate = value; }
            }

            public DateTime testToDate
            {
                get { return base.toDate;  }
                set { base.toDate = value; }
            }

            public decimal testMoneyPool
            {
                get { return base.moneyPool;  }
                set { base.moneyPool = value; }
            }

            public decimal testAllocated
            {
                get { return base.allocated;  }
                set { base.allocated = value; }
            }

            public decimal testUnallocated
            {
                get { return base.unallocated;  }
                set { base.unallocated = value; }
            }

            public decimal testTotalSpent
            {
                get { return base.totalSpent; }
            }

            public decimal testBalance
            {
                get { return base.balance; }
            }

            public Dictionary<String, Allocation> testAllocations
            {
                get { return base.allocations; }
            }
        }
        #endregion
    }
}

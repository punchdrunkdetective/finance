﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Finance;

namespace UnitTests
{
    [TestClass]
    public class AllocationTests
    {
        // TODO: Generalize the testing proccedure for testing both the sucess and failure of making an Expense. (low priority)

        #region Sucessfull Construction
        [TestMethod]
        public void TestConstruction()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            Assert.IsNotNull( testAlloc,               "Constructor returned null" );
            Assert.IsNotNull( testAlloc.testExpenses,  "Transaction list is null." );
            Assert.IsNotNull( testAlloc.GetExpenses(), "Transaction list is null." );

            Assert.AreEqual( "testAlloc",                 testAlloc.testName,            "Incorrect Allocation name."           );
            Assert.AreEqual( 40.00m,                      testAlloc.testInitAmt,         "Incorrect Allocation intial amount."  );
            Assert.AreEqual( 40.00m,                      testAlloc.testCurrAmt,         "Incorrect Allocation current amount." );
            Assert.AreEqual( DateTime.Today,              testAlloc.testFromDate,        "Incorrect Allocation start date."     );
            Assert.AreEqual( DateTime.Today.AddDays( 2 ), testAlloc.testToDate,          "Incorrect Allocation end date."       );
            Assert.AreEqual( 0,                           testAlloc.testExpenses.Count,  "Incorrect Allocation expense count."  );
            Assert.AreEqual( 0,                           testAlloc.GetExpenses().Count, "Incorrect Allocation expense count."  );
        }

        [TestMethod]
        public void TestConstructionWithOneDayRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today );

            Assert.IsNotNull( testAlloc,               "Constructor returned null" );
            Assert.IsNotNull( testAlloc.testExpenses,  "Transaction list is null." );
            Assert.IsNotNull( testAlloc.GetExpenses(), "Transaction list is null." );

            Assert.AreEqual( "testAlloc",    testAlloc.testName,            "Incorrect Allocation name."           );
            Assert.AreEqual( 40.00m,         testAlloc.testInitAmt,         "Incorrect Allocation intial amount."  );
            Assert.AreEqual( 40.00m,         testAlloc.testCurrAmt,         "Incorrect Allocation current amount." );
            Assert.AreEqual( DateTime.Today, testAlloc.testFromDate,        "Incorrect Allocation start date."     );
            Assert.AreEqual( DateTime.Today, testAlloc.testToDate,          "Incorrect Allocation end date."       );
            Assert.AreEqual( 0,              testAlloc.testExpenses.Count,  "Incorrect Allocation expense count."  );
            Assert.AreEqual( 0,              testAlloc.GetExpenses().Count, "Incorrect Allocation expense count."  );
        }
        #endregion

        #region Failed Construction
        [TestMethod]
        public void TestFialureToConstructWithNegativeValue()
        {
            ExposedAllocation testAlloc       = null;
            ArgumentException negativeInitAmt = null;
            try
            {
                testAlloc = new ExposedAllocation( "failure", -40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            }
            catch ( ArgumentException e )
            {
                negativeInitAmt = e;
            }

            Assert.IsNull( testAlloc, "Construction did not return null." );
            Assert.IsNotNull( negativeInitAmt, "Exception was not caught." );
        }

        [TestMethod]
        public void TestFailureToConstructWithInvalidDateRange()
        {
            ExposedAllocation testAlloc        = null;
            ArgumentException invalidDateRange = null;
            try
            {
                testAlloc = new ExposedAllocation( "failure", -40.00m, DateTime.Today.AddDays( 2 ), DateTime.Today );
            }
            catch (ArgumentException e )
            {
                invalidDateRange = e;
            }

            Assert.IsNull( testAlloc, "Construction did not return null." );
            Assert.IsNotNull( invalidDateRange, "Exception was not caught." );
        }
        #endregion

        #region Sucessfull SetProperties
        [TestMethod]
        public void TestSucessfullSetName()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            testAlloc.testName = "newName";

            Assert.AreEqual( "newName", testAlloc.testName, "Name did not change." );
        }

        [TestMethod]
        public void TestSucessfullSetInitAmtToGreaterValue()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today, "test expense", 10.00m );

            testAlloc.testInitAmt = 60.00m;

            Assert.AreEqual( 60.00m, testAlloc.testInitAmt, "Initial amount did not increase correctly." );
            Assert.AreEqual( 50.00m, testAlloc.testCurrAmt, "Current amount did not increase correctly with increase of initial amount." );
        }

        [TestMethod]
        public void TestSucessfullSetInitAmtToLesserValue()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today, "test expense", 10.00m );

            testAlloc.testInitAmt = 0.00m;

            Assert.AreEqual( 0.00m,   testAlloc.testInitAmt, "Initial amount did not decrease correctly." );
            Assert.AreEqual( -10.00m, testAlloc.testCurrAmt, "Current amount did not decrease correctly with decrease of initial amount." );
        }
        
        [TestMethod]
        public void TestSucessfullSetCurrAmt()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            testAlloc.testCurrAmt = 10.00m;

            Assert.AreEqual( 10.00m, testAlloc.testCurrAmt, "Current amount did not update." );
        }

        [TestMethod]
        public void TestSucessfullSetFromDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            testAlloc.testFromDate = DateTime.Today.AddDays( 2 );

            Assert.AreEqual( DateTime.Today.AddDays( 2 ), testAlloc.testFromDate, "From date did not change correctly." );
        }

        [TestMethod]
        public void TestSucessfullSetToDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            testAlloc.testToDate = DateTime.Today;

            Assert.AreEqual( DateTime.Today, testAlloc.testToDate, "To date did not change correctly." );
        }

        [TestMethod]
        public void TestSucessfullRemovalOfExpensesWhenSettingFromDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today,              "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            int     newExpenseCount = testAlloc.GetExpenses().Count - 1;
            decimal newCurAmt       = testAlloc.currAmt + testAlloc.GetExpenses()[0].amt;

            testAlloc.testFromDate = testAlloc.fromDate.AddDays( 1 );

            Assert.AreEqual( newExpenseCount, testAlloc.GetExpenses().Count, "Expense was not removed from Allocation's list." );
            Assert.AreEqual( newCurAmt,       testAlloc.currAmt,             "Allocation's currAmt was not reduced."           );
        }

        [TestMethod]
        public void TestSucessfullRemovalOfExpensesWhenSettingToDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today,              "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            int     newExpenseCount = testAlloc.GetExpenses().Count - 1;
            decimal newCurAmt       = testAlloc.currAmt + testAlloc.GetExpenses()[2].amt;

            testAlloc.testToDate = testAlloc.toDate.AddDays( -1 );

            Assert.AreEqual( newExpenseCount, testAlloc.GetExpenses().Count, "Expense was not removed from Allocation's list." );
            Assert.AreEqual( newCurAmt,       testAlloc.currAmt,             "Allocation's currAmt was not reduced."           );
        }
        #endregion

        #region Failed SetProperties
        [TestMethod]
        public void TestFailureToSetInitAmtToNegativeValue()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today, "test expense", 10.00m );

            ArgumentException negativeInitAmt = null;
            try
            {
                testAlloc.testInitAmt = -10.00m;
            }
            catch (ArgumentException e)
            {
                negativeInitAmt = e;
            }

            Assert.IsNotNull( negativeInitAmt, "Exception was not caught." );

            Assert.AreEqual( 40.00m, testAlloc.testInitAmt, "Initial amount decreased" );
            Assert.AreEqual( 30.00m, testAlloc.testCurrAmt, "Current amount decreased with decrease of initial amount." );
        }

        [TestMethod]
        public void TestFailureToSetFromDateAfterToDate()
        {
            ExposedAllocation testAlloc   = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            ArgumentException invalidDate = null;

            try
            {
                testAlloc.testFromDate = testAlloc.testToDate.AddDays( 1 );
            }
            catch (ArgumentException e)
            {
                invalidDate = e;
            }

            Assert.IsNotNull( invalidDate, "fromDate set to after toDate." );
        }

        [TestMethod]
        public void TestFailureToSetToDateBeforeFromDate()
        {
            ExposedAllocation testAlloc   = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            ArgumentException invalidDate = null;

            try
            {
                testAlloc.testToDate = testAlloc.testFromDate.AddDays( -1 );
            }
            catch (ArgumentException e)
            {
                invalidDate = e;
            }

            Assert.IsNotNull( invalidDate, "toDate set to before fromDate." );
        }
        #endregion

        #region Sucessfull MakeExpense
        [TestMethod]
        public void TestMakeExpense()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            bool expenseMade = false;
            expenseMade = testAlloc.MakeExpense( DateTime.Today,              "test expense 1", 10.00m );
            expenseMade = testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "test expense 2", 20.00m );
            expenseMade = testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "test expense 3", 20.00m );

            Assert.IsTrue( expenseMade, "An expense was not sucessfully made." );

            Assert.AreEqual( -10.00m, testAlloc.currAmt,             "Allocation was not reduced by transaction amount correctly."           );
            Assert.AreEqual( 3,       testAlloc.GetExpenses().Count, "Transactions were not successfully added to internal list of expesnes" );
        }
        #endregion

        #region Failed MakeExpense
        [TestMethod]
        public void TestFailureToMakeNegativeExpense()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            bool expenseMade = testAlloc.MakeExpense( DateTime.Today, "fail expense test", -10.00m );

            Assert.IsFalse( expenseMade, "Expense of negative worth was allowed to be made." );

            Assert.AreEqual( 40.00m, testAlloc.currAmt,             "Allocation was reduced by transaction amount." );
            Assert.AreEqual( 0,      testAlloc.GetExpenses().Count, "Transaction was added to list of expenses."    );
        }

        [TestMethod]
        public void TestFailureToMakeExpenseBeforeDateRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            bool expenseMade = testAlloc.MakeExpense( DateTime.Today.AddDays( -1 ), "fail expense test", 10.00m );

            Assert.IsFalse( expenseMade, "an expense below the transaction range was allowed" );

            Assert.AreEqual( 40.00m, testAlloc.currAmt,             "Allocation was reduced by transaction amount." );
            Assert.AreEqual( 0,      testAlloc.GetExpenses().Count, "Transactions were added to list of expenses"   );
        }

        [TestMethod]
        public void TestFailureToMakeExpenseAfterDateRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );

            bool expenseMade = testAlloc.MakeExpense( DateTime.Today.AddDays( 3 ), "fail expense test", 10.00m );

            Assert.IsFalse( expenseMade, "an expense above the transaction range was allowed" );
               
            Assert.AreEqual( 40.00m, testAlloc.currAmt,             "Allocation was reduced by transaction amount." );
            Assert.AreEqual( 0,      testAlloc.GetExpenses().Count, "Transactions were added to list of expenses"   );
        }
        #endregion

        #region Sucessfull UpdateExpense
        [TestMethod]
        public void TestSucessfullUpdateExpenseDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime newDate              = DateTime.Today;
            String   oldDescription       = "testExpense3";
            decimal  oldAmount            = 10.00m;

            this.TestSucessToUpdateExpenseDate( testAlloc, transactionUnderTest, newDate, oldDescription, oldAmount );
        }

        [TestMethod]
        public void TestSucessfullUpdateExpenseDescription()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   newDescription       = "testExpense3_updated";
            decimal  oldAmount            = 10.00m;

            this.TestSucessToUpdateExpenseDescription( testAlloc, transactionUnderTest, oldDate, newDescription, oldAmount );
        }

        [TestMethod]
        public void TestSucessfullUpdateExpenseWithAmountIncrease()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   oldDescription       = "testExpense3";
            decimal  newAmount            = 20.00m;

            this.TestSucessToUpdateExpenseAmount( testAlloc, transactionUnderTest, oldDate, oldDescription, newAmount );
        }

        [TestMethod]
        public void TestSucessfullUpdateExpenseWithAmountDecrease()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   oldDescription       = "testExpense3";
            decimal  newAmount            = 0.00m;

            this.TestSucessToUpdateExpenseAmount( testAlloc, transactionUnderTest, oldDate, oldDescription, newAmount );
        }
        #endregion

        #region Failed UpdateExpense
        [TestMethod]
        public void TestFailureToUpdateExpenseWithDateBeforeFromDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime newDate              = DateTime.Today.AddDays( -1 );
            String   oldDescription       = "testExpense3";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDate( testAlloc, transactionUnderTest, newDate, oldDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseWithDateAfterTooDate()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime newDate              = DateTime.Today.AddDays( 3 );
            String   oldDescription       = "testExpense3";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDate( testAlloc, transactionUnderTest, newDate, oldDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseWithNegativeAmount()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 2;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   oldDescription       = "testExpense3";
            decimal  newAmount            = -20.00m;

            this.TestFailureToUpdateExpenseAmount( testAlloc, transactionUnderTest, oldDate, oldDescription, newAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseDateBelowIndexRange()
        {
            Allocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = -1;
            DateTime newDate              = DateTime.Today;
            String   oldDescription       = "testExpense3";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDate( testAlloc, transactionUnderTest, newDate, oldDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseDateAboveIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 3;
            DateTime newDate              = DateTime.Today;
            String   oldDescription       = "testExpense3";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDate( testAlloc, transactionUnderTest, newDate, oldDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseDescriptionBelowIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = -1;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   newDescription       = "testExpense_updated";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDescription( testAlloc, transactionUnderTest, oldDate, newDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseDescriptionAboveIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 3;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   newDescription       = "testExpense_updated";
            decimal  oldAmount            = 10.00m;

            this.TestFailureToUpdateExpenseDescription( testAlloc, transactionUnderTest, oldDate, newDescription, oldAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseAmountBelowIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = -1;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   oldDescription       = "testExpense3";
            decimal  newAmount            = 20.00m;

            this.TestFailureToUpdateExpenseAmount( testAlloc, transactionUnderTest, oldDate, oldDescription, newAmount );
        }

        [TestMethod]
        public void TestFailureToUpdateExpenseAmountAboveIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 40.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 10.00m );

            int      transactionUnderTest = 3;
            DateTime oldDate              = DateTime.Today.AddDays( 2 );
            String   oldDescription       = "testExpense3";
            decimal  newAmount            = 20.00m;

            this.TestFailureToUpdateExpenseAmount( testAlloc, transactionUnderTest, oldDate, oldDescription, newAmount );
        }
        #endregion

        #region Sucessfull RemoveExpense
        [TestMethod]
        public void TestSucessfullRemovalOfFirstExpense()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            bool sucess = testAlloc.RemoveExpense( 0 );

            Assert.IsTrue( sucess, "RemoveExpense returned false." );

            Assert.AreEqual( 2,  testAlloc.GetExpenses().Count, "Expense was not removed."               );
            Assert.AreEqual( 50, testAlloc.currAmt,             "Allocation's currAmt did not decrease." );
        }

        [TestMethod]
        public void TestSucessfullRemovalOfMiddleExpense()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            bool sucess = testAlloc.RemoveExpense( 1 );

            Assert.IsTrue( sucess, "RemoveExpense returned false." );

            Assert.AreEqual( 2,  testAlloc.GetExpenses().Count, "Expense was not removed."               );
            Assert.AreEqual( 60, testAlloc.currAmt,             "Allocation's currAmt did not decrease." );
        }

        [TestMethod]
        public void TestSucessfullRemovalOfLaseExpense()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            bool sucess = testAlloc.RemoveExpense( 2 );

            Assert.IsTrue( sucess, "RemoveExpense returned false." );

            Assert.AreEqual( 2,  testAlloc.GetExpenses().Count, "Expense was not removed."               );
            Assert.AreEqual( 70, testAlloc.currAmt,             "Allocation's currAmt did not decrease." );
        }
        #endregion

        #region Failed RemoveExpense
        [TestMethod]
        public void TestFailureToRemoveExpenseBelowIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            bool sucess = testAlloc.RemoveExpense( -1 );

            Assert.IsFalse( sucess, "RemoveExpense returned true." );

            Assert.AreEqual( 3,      testAlloc.GetExpenses().Count, "Expense was removed."     );
            Assert.AreEqual( 40.00m, testAlloc.currAmt,             "Current amount increase." );
        }

        [TestMethod]
        public void TestFailureToRemoveExpenseAboveIndexRange()
        {
            ExposedAllocation testAlloc = new ExposedAllocation( "testAlloc", 100.00m, DateTime.Today, DateTime.Today.AddDays( 2 ) );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 0 ), "testExpense1", 10.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 1 ), "testExpense2", 20.00m );
            testAlloc.MakeExpense( DateTime.Today.AddDays( 2 ), "testExpense3", 30.00m );

            bool sucess = testAlloc.RemoveExpense( 3 );

            Assert.IsFalse( sucess, "RemoveExpense returned true." );

            Assert.AreEqual( 3,      testAlloc.GetExpenses().Count, "Expense was removed."     );
            Assert.AreEqual( 40.00m, testAlloc.currAmt,             "Current amount increase." );
        }
        #endregion

        #region Utilities
        /// <summary>
        /// This is a utility method to test that a expense sucessfully updated.
        /// </summary>
        public void TestSucessToUpdateExpenseDate( Allocation testAlloc, int transactionUnderTest, DateTime newDate, String oldDescription, decimal oldAmount )
        {
            decimal oldInitAmt = testAlloc.initAmt;
            decimal oldCurrAmt = testAlloc.currAmt;

            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newDate );
            Assert.IsTrue( expenseUpdated, "UpdateExpense returned false." );

            Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
            Assert.AreEqual( newDate,        updatedExpense.date,  "Date was not updated."    );
            Assert.AreEqual( oldDescription, updatedExpense.descr, "Description was updated." );
            Assert.AreEqual( oldAmount,      updatedExpense.amt,   "Amount was updated"       );

            Assert.AreEqual( oldInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during update." );
            Assert.AreEqual( oldCurrAmt, testAlloc.currAmt, "Associated Allocation's currAmt was updated during update." );
        }

        /// <summary>
        /// This is a utility method to test that a expense failed to update.
        /// </summary>
        public void TestFailureToUpdateExpenseDate( Allocation testAlloc, int transactionUnderTest, DateTime newDate, String oldDescription, decimal oldAmount )
        {
            // The following values should not change during the failed update.
            decimal originalAllocInitAmt = testAlloc.initAmt;
            decimal originalAllocCurrAmt = testAlloc.currAmt;

            // The following values are only used and assigned to proper value if transactionUnderTest is a valid index.
            DateTime oldDate = DateTime.MinValue;
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                // We are wanting to test that the following values will not change upon failure.
                oldDate = testAlloc.GetExpenses()[transactionUnderTest].date;
            }

            // Test that the update method reports a failure.
            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newDate );
            Assert.IsFalse( expenseUpdated, "UpdateExpense returned true." );

            // Test that the transaction under test did NOT have any of its values changed.
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
                Assert.AreEqual( oldDate,        updatedExpense.date,  "Date was updated."        );
                Assert.AreEqual( oldDescription, updatedExpense.descr, "Description was updated." );
                Assert.AreEqual( oldAmount,      updatedExpense.amt,   "Amount was updated"       );
            }

            // Test that the allocation under test did NOT change.
            Assert.AreEqual( originalAllocInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during failed update." );
            Assert.AreEqual( originalAllocCurrAmt, testAlloc.currAmt, "Associated Allocation's currAmt was updated during failed update." );
        }

        /// <summary>
        /// This is a utility method to test that a expense sucessfully updated.
        /// </summary>
        public void TestSucessToUpdateExpenseDescription( Allocation testAlloc, int transactionUnderTest, DateTime oldDate, String newDescription, decimal oldAmount )
        {
            decimal oldInitAmt = testAlloc.initAmt;
            decimal oldCurrAmt = testAlloc.currAmt;

            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newDescription );
            Assert.IsTrue( expenseUpdated, "UpdateExpense returned false." );

            Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
            Assert.AreEqual( oldDate,        updatedExpense.date,  "Date was updated."            );
            Assert.AreEqual( newDescription, updatedExpense.descr, "Description was not updated." );
            Assert.AreEqual( oldAmount,      updatedExpense.amt,   "Amount was updated"           );

            Assert.AreEqual( oldInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during update." );
            Assert.AreEqual( oldCurrAmt, testAlloc.currAmt, "Associated Allocation's currAmt was changed during update." );
        }

        /// <summary>
        /// This is a utility method to test that a expense failed to update.
        /// </summary>
        public void TestFailureToUpdateExpenseDescription( Allocation testAlloc, int transactionUnderTest, DateTime oldDate, String newDescription, decimal oldAmount )
        {
            // The following values should not change during the failed update.
            decimal originalAllocInitAmt = testAlloc.initAmt;
            decimal originalAllocCurrAmt = testAlloc.currAmt;

            // The following values are only used and assigned to proper value if transactionUnderTest is a valid index.
            String oldDescription = "";
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                // We are wanting to test that the following values will not change upon failure.
                oldDescription = testAlloc.GetExpenses()[transactionUnderTest].descr;
            }

            // Test that the update method reports a failure.
            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newDescription );
            Assert.IsFalse( expenseUpdated, "UpdateExpense returned true." );

            // Test that the transaction under test did NOT have any of its values changed.
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
                Assert.AreEqual( oldDate,        updatedExpense.date,  "Date was updated."        );
                Assert.AreEqual( oldDescription, updatedExpense.descr, "Description was updated." );
                Assert.AreEqual( oldAmount,      updatedExpense.amt,   "Amount was updated"       );
            }

            // Test that the allocation under test did NOT change.
            Assert.AreEqual( originalAllocInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during failed update." );
            Assert.AreEqual( originalAllocCurrAmt, testAlloc.currAmt, "Associated Allocation's currAmt was updated during failed update." );
        }

        /// <summary>
        /// This is a utility method to test that a expense sucessfully updated.
        /// </summary>
        public void TestSucessToUpdateExpenseAmount( Allocation testAlloc, int transactionUnderTest, DateTime oldDate, String oldDescription, decimal newAmount )
        {
            decimal originalInitAmt = testAlloc.initAmt;

            decimal newCurrAmt;
            decimal transactionAmtDff = (Math.Abs( testAlloc.GetExpenses()[transactionUnderTest].amt - newAmount ));
            if (newAmount > testAlloc.GetExpenses()[transactionUnderTest].amt)
            {
                // An increase in Transaction amount should cause a decrease in currAmt
                newCurrAmt = testAlloc.currAmt - transactionAmtDff;
            }
            else if (newAmount < testAlloc.GetExpenses()[transactionUnderTest].amt)
            {
                // A decrease in Transaction amount should cause a increase in currAmt 
                newCurrAmt = testAlloc.currAmt + transactionAmtDff;
            }
            else
            {
                newCurrAmt = testAlloc.currAmt;
            }

            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newAmount );
            Assert.IsTrue( expenseUpdated, "UpdateExpense returned false." );

            Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
            Assert.AreEqual( oldDate,        updatedExpense.date,  "Date was updated."        );
            Assert.AreEqual( oldDescription, updatedExpense.descr, "Description was updated." );
            Assert.AreEqual( newAmount,      updatedExpense.amt,   "Amount was not updated"   );

            Assert.AreEqual( originalInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during update." );
            Assert.AreEqual( newCurrAmt,      testAlloc.currAmt, "Associated Allocation's currAmt was not updated correctly." );
        }

        /// <summary>
        /// This is a utility method to test that a expense failed to update.
        /// </summary>
        public void TestFailureToUpdateExpenseAmount( Allocation testAlloc, int transactionUnderTest, DateTime oldDate, String oldDescription, decimal newAmount )
        {
            // The following values should not change during the failed update.
            decimal originalAllocInitAmt = testAlloc.initAmt;
            decimal originalAllocCurrAmt = testAlloc.currAmt;

            // The following values are only used and assigned to proper value if transactionUnderTest is a valid index.
            decimal  oldAmount = 0.00m;
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                // We are wanting to test that the following values will not change upon failure.
                oldAmount = testAlloc.GetExpenses()[transactionUnderTest].amt;
            }

            // Test that the update method reports a failure.
            bool expenseUpdated = testAlloc.UpdateExpense( transactionUnderTest, newAmount );
            Assert.IsFalse( expenseUpdated, "UpdateExpense returned true." );

            // Test that the transaction under test did NOT have any of its values changed.
            if (0 <= transactionUnderTest && transactionUnderTest < testAlloc.GetExpenses().Count)
            {
                Transaction updatedExpense = testAlloc.GetExpenses()[transactionUnderTest];
                Assert.AreEqual( oldDate,        updatedExpense.date,  "Date was updated."        );
                Assert.AreEqual( oldDescription, updatedExpense.descr, "Description was updated." );
                Assert.AreEqual( oldAmount,      updatedExpense.amt,   "Amount was updated"       );
            }

            // Test that the allocation under test did NOT change.
            Assert.AreEqual( originalAllocInitAmt, testAlloc.initAmt, "Assocaited Allocation's initAmt was changed during failed update." );
            Assert.AreEqual( originalAllocCurrAmt, testAlloc.currAmt, "Associated Allocation's currAmt was updated during failed update." );
        }

        /// <summary>
        /// A test utility class that exposes hidden elements of an Allocation. The convention used is to use this class primary as a means of construction.
        /// Only access any exposed properties or methods while testing the construcion of an Allocation or while testing the setters and getters of any 
        /// hidden properties. Other than that, the normal public interface of Allocation should be used for testing to avoid any false positives that 
        /// might incure with manipulation of hidden state. 
        /// </summary>
        private class ExposedAllocation : Allocation
        {
            public ExposedAllocation( string name, decimal initAmt, DateTime fromDate, DateTime toDate ) 
                : base( name, initAmt, fromDate, toDate ) 
            {
                // Do nothing.
            }

            public String testName
            {
                get { return base.name;  }
                set { base.name = value; }
            }

            public decimal testInitAmt
            {
                get { return base.initAmt;  }
                set { base.initAmt = value; }
            }

            public decimal testCurrAmt
            {
                get { return base.currAmt;  }
                set { base.currAmt = value; }
            }

            public DateTime testFromDate
            {
                get { return base.fromDate;  }
                set { base.fromDate = value; }
            }

            public DateTime testToDate
            {
                get { return base.toDate;  }
                set { base.toDate = value; }
            }

            public List<Transaction> testExpenses
            {
                get { return base.expenses; }
            }
        }
        #endregion
    }
}
